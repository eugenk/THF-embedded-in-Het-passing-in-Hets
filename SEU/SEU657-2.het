logic THF

spec SEU6572 =
%------------------------------------------------------------------------------
% File     : SEU657^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Ordered Pairs - Properties of Pairs
% Version  : Especial > Reduced > Especial.
% English  : (! u:i.iskpair u -> kpair (kfst u) (ksnd u) = u)

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC159l [Bro08]

% Status   : Theorem
% Rating   : 0.29 v6.1.0, 0.43 v5.5.0, 0.33 v5.4.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.40 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   15 (   3 unit;  10 type;   4 defn)
%            Number of atoms       :   83 (   8 equality;  21 variable)
%            Maximal formula depth :   14 (   5 average)
%            Number of connectives :   42 (   0   ~;   0   |;   2   &;  37   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   12 (  10   :)
%            Number of variables   :   10 (   0 sgn;   5   !;   2   ?;   3   ^)
%                                         (  10   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=426
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(emptyset_type,type,(
    emptyset: $i )).

thf(setadjoin_type,type,(
    setadjoin: $i > $i > $i )).

thf(setunion_type,type,(
    setunion: $i > $i )).

thf(iskpair_type,type,(
    iskpair: $i > $o )).

thf(iskpair,definition,
    ( iskpair
    = ( ^ [A: $i] :
        ? [Xx: $i] :
          ( ( in @ Xx @ ( setunion @ A ) )
          & ? [Xy: $i] :
              ( ( in @ Xy @ ( setunion @ A ) )
              & ( A
                = ( setadjoin @ ( setadjoin @ Xx @ emptyset ) @ ( setadjoin @ ( setadjoin @ Xx @ ( setadjoin @ Xy @ emptyset ) ) @ emptyset ) ) ) ) ) ) )).

thf(kpair_type,type,(
    kpair: $i > $i > $i )).

thf(kpair,definition,
    ( kpair
    = ( ^ [Xx: $i,Xy: $i] :
          ( setadjoin @ ( setadjoin @ Xx @ emptyset ) @ ( setadjoin @ ( setadjoin @ Xx @ ( setadjoin @ Xy @ emptyset ) ) @ emptyset ) ) ) )).

thf(kfst_type,type,(
    kfst: $i > $i )).

thf(kfstpairEq_type,type,(
    kfstpairEq: $o )).

thf(kfstpairEq,definition,
    ( kfstpairEq
    = ( ! [Xx: $i,Xy: $i] :
          ( ( kfst @ ( kpair @ Xx @ Xy ) )
          = Xx ) ) )).

thf(ksnd_type,type,(
    ksnd: $i > $i )).

thf(ksndpairEq_type,type,(
    ksndpairEq: $o )).

thf(ksndpairEq,definition,
    ( ksndpairEq
    = ( ! [Xx: $i,Xy: $i] :
          ( ( ksnd @ ( kpair @ Xx @ Xy ) )
          = Xy ) ) )).

thf(kpairsurjEq,conjecture,
    ( kfstpairEq
   => ( ksndpairEq
     => ! [Xu: $i] :
          ( ( iskpair @ Xu )
         => ( ( kpair @ ( kfst @ Xu ) @ ( ksnd @ Xu ) )
            = Xu ) ) ) )).

%------------------------------------------------------------------------------
