logic THF

spec SEU9395 =
%------------------------------------------------------------------------------
% File     : SEU939^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Functions)
% Problem  : TPS problem THM112B
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0366 [Bro09]
%          : THM112B [TPS]

% Status   : Theorem
% Rating   : 0.83 v6.1.0, 0.67 v5.5.0, 0.80 v5.4.0, 0.75 v5.3.0, 1.00 v5.2.0, 0.50 v4.1.0, 0.67 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :   33 (   0 equality;  33 variable)
%            Maximal formula depth :   16 (  16 average)
%            Number of connectives :   32 (   0   ~;   4   |;   3   &;  23   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    9 (   9   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    2 (   0   :)
%            Number of variables   :   14 (   0 sgn;   8   !;   2   ?;   4   ^)
%                                         (  14   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cTHM112B,conjecture,(
    ! [P: $i > $o] :
    ? [Xm_9: ( $i > $i ) > $i > $o,Xm_10: ( $i > $i ) > $i > $o] :
      ( ! [Xw_1: $i] :
          ( ( Xm_9
            @ ^ [Xx: $i] : Xx
            @ Xw_1 )
          | ( Xm_10
            @ ^ [Xx: $i] : Xx
            @ Xw_1 ) )
      & ! [G: $i > $i,H: $i > $i] :
          ( ( ! [Xw_1: $i] :
                ( ( Xm_9 @ G @ Xw_1 )
                | ( Xm_10 @ G @ Xw_1 ) )
            & ! [Xw_1: $i] :
                ( ( Xm_9 @ H @ Xw_1 )
                | ( Xm_10 @ H @ Xw_1 ) ) )
         => ( ! [Xw_1: $i] :
                ( ( Xm_9
                  @ ^ [Xx: $i] :
                      ( G @ ( H @ Xx ) )
                  @ Xw_1 )
                | ( Xm_10
                  @ ^ [Xx: $i] :
                      ( G @ ( H @ Xx ) )
                  @ Xw_1 ) )
            & ! [Y: $i] :
                ( ( P @ Y )
               => ( P @ ( G @ Y ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
