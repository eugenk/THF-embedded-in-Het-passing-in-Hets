logic THF

spec SEU6902 =
%------------------------------------------------------------------------------
% File     : SEU690^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Functions - Extensionality and Beta Reduction
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! B:i.! R:i.breln A B R -> (! S:i.breln A B S ->
%            (! x:i.in x A -> (! y:i.in y B -> in (kpair x y) R ->
%            in (kpair x y) S)) -> (! x:i.in x A -> (! y:i.in y B ->
%            in (kpair x y) S -> in (kpair x y) R)) -> R = S))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC192l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :   11 (   2 unit;   7 type;   3 defn)
%            Number of atoms       :  110 (   5 equality;  55 variable)
%            Maximal formula depth :   20 (   7 average)
%            Number of connectives :   78 (   0   ~;   0   |;   0   &;  58   @)
%                                         (   0 <=>;  20  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   11 (  11   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    9 (   7   :)
%            Number of variables   :   19 (   0 sgn;  16   !;   0   ?;   3   ^)
%                                         (  19   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=244
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(subset_type,type,(
    subset: $i > $i > $o )).

thf(setextsub_type,type,(
    setextsub: $o )).

thf(setextsub,definition,
    ( setextsub
    = ( ! [A: $i,B: $i] :
          ( ( subset @ A @ B )
         => ( ( subset @ B @ A )
           => ( A = B ) ) ) ) )).

thf(kpair_type,type,(
    kpair: $i > $i > $i )).

thf(cartprod_type,type,(
    cartprod: $i > $i > $i )).

thf(breln_type,type,(
    breln: $i > $i > $i > $o )).

thf(breln,definition,
    ( breln
    = ( ^ [A: $i,B: $i,C: $i] :
          ( subset @ C @ ( cartprod @ A @ B ) ) ) )).

thf(subbreln_type,type,(
    subbreln: $o )).

thf(subbreln,definition,
    ( subbreln
    = ( ! [A: $i,B: $i,R: $i] :
          ( ( breln @ A @ B @ R )
         => ! [S: $i] :
              ( ( breln @ A @ B @ S )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ! [Xy: $i] :
                        ( ( in @ Xy @ B )
                       => ( ( in @ ( kpair @ Xx @ Xy ) @ R )
                         => ( in @ ( kpair @ Xx @ Xy ) @ S ) ) ) )
               => ( subset @ R @ S ) ) ) ) ) )).

thf(eqbreln,conjecture,
    ( setextsub
   => ( subbreln
     => ! [A: $i,B: $i,R: $i] :
          ( ( breln @ A @ B @ R )
         => ! [S: $i] :
              ( ( breln @ A @ B @ S )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ! [Xy: $i] :
                        ( ( in @ Xy @ B )
                       => ( ( in @ ( kpair @ Xx @ Xy ) @ R )
                         => ( in @ ( kpair @ Xx @ Xy ) @ S ) ) ) )
               => ( ! [Xx: $i] :
                      ( ( in @ Xx @ A )
                     => ! [Xy: $i] :
                          ( ( in @ Xy @ B )
                         => ( ( in @ ( kpair @ Xx @ Xy ) @ S )
                           => ( in @ ( kpair @ Xx @ Xy ) @ R ) ) ) )
                 => ( R = S ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
