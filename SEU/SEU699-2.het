logic THF

spec SEU6992 =
%------------------------------------------------------------------------------
% File     : SEU699^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Functions - Extensionality and Beta Reduction
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! B:i.! f:i.in f (funcSet A B) ->
%            lam A B (^ x:i.ap A B f x) = f)

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC201l [Bro08]

% Status   : Theorem
% Rating   : 0.43 v6.1.0, 0.29 v6.0.0, 0.43 v5.5.0, 0.50 v5.4.0, 0.60 v4.1.0, 1.00 v4.0.1, 0.67 v4.0.0, 1.00 v3.7.0
% Syntax   : Number of formulae    :   13 (   4 unit;   8 type;   4 defn)
%            Number of atoms       :  127 (   8 equality;  65 variable)
%            Maximal formula depth :   16 (   8 average)
%            Number of connectives :   86 (   0   ~;   0   |;   0   &;  70   @)
%                                         (   0 <=>;  16  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   14 (  14   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   10 (   8   :)
%            Number of variables   :   24 (   0 sgn;  21   !;   0   ?;   3   ^)
%                                         (  24   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=251
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(funcSet_type,type,(
    funcSet: $i > $i > $i )).

thf(ap_type,type,(
    ap: $i > $i > $i > $i > $i )).

thf(ap2p_type,type,(
    ap2p: $o )).

thf(ap2p,definition,
    ( ap2p
    = ( ! [A: $i,B: $i,Xf: $i] :
          ( ( in @ Xf @ ( funcSet @ A @ B ) )
         => ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( in @ ( ap @ A @ B @ Xf @ Xx ) @ B ) ) ) ) )).

thf(lam_type,type,(
    lam: $i > $i > ( $i > $i ) > $i )).

thf(lam2p_type,type,(
    lam2p: $o )).

thf(lam2p,definition,
    ( lam2p
    = ( ! [A: $i,B: $i,Xf: $i > $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( in @ ( Xf @ Xx ) @ B ) )
         => ( in
            @ ( lam @ A @ B
              @ ^ [Xx: $i] :
                  ( Xf @ Xx ) )
            @ ( funcSet @ A @ B ) ) ) ) )).

thf(funcext2_type,type,(
    funcext2: $o )).

thf(funcext2,definition,
    ( funcext2
    = ( ! [A: $i,B: $i,Xf: $i] :
          ( ( in @ Xf @ ( funcSet @ A @ B ) )
         => ! [Xg: $i] :
              ( ( in @ Xg @ ( funcSet @ A @ B ) )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ( ( ap @ A @ B @ Xf @ Xx )
                      = ( ap @ A @ B @ Xg @ Xx ) ) )
               => ( Xf = Xg ) ) ) ) ) )).

thf(beta2_type,type,(
    beta2: $o )).

thf(beta2,definition,
    ( beta2
    = ( ! [A: $i,B: $i,Xf: $i > $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( in @ ( Xf @ Xx ) @ B ) )
         => ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( ( ap @ A @ B
                  @ ( lam @ A @ B
                    @ ^ [Xy: $i] :
                        ( Xf @ Xy ) )
                  @ Xx )
                = ( Xf @ Xx ) ) ) ) ) )).

thf(eta2,conjecture,
    ( ap2p
   => ( lam2p
     => ( funcext2
       => ( beta2
         => ! [A: $i,B: $i,Xf: $i] :
              ( ( in @ Xf @ ( funcSet @ A @ B ) )
             => ( ( lam @ A @ B
                  @ ^ [Xx: $i] :
                      ( ap @ A @ B @ Xf @ Xx ) )
                = Xf ) ) ) ) ) )).

%------------------------------------------------------------------------------
