logic THF

spec SEU5462 =
%------------------------------------------------------------------------------
% File     : SEU546^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Preliminary Notions - Equivalence Laws
% Version  : Especial > Reduced > Especial.
% English  : (! phi:i>o.(? x:i.! y:i.phi y <-> y = x) -> exu (^ x:i.phi x))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC048l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    5 (   1 unit;   2 type;   2 defn)
%            Number of atoms       :   34 (   5 equality;  20 variable)
%            Maximal formula depth :    9 (   6 average)
%            Number of connectives :   17 (   0   ~;   0   |;   2   &;   9   @)
%                                         (   1 <=>;   5  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :)
%            Number of variables   :   11 (   0 sgn;   5   !;   3   ?;   3   ^)
%                                         (  11   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=409
%          : 
%------------------------------------------------------------------------------
thf(exu_type,type,(
    exu: ( $i > $o ) > $o )).

thf(exu,definition,
    ( exu
    = ( ^ [Xphi: $i > $o] :
        ? [Xx: $i] :
          ( ( Xphi @ Xx )
          & ! [Xy: $i] :
              ( ( Xphi @ Xy )
             => ( Xx = Xy ) ) ) ) )).

thf(exuI1_type,type,(
    exuI1: $o )).

thf(exuI1,definition,
    ( exuI1
    = ( ! [Xphi: $i > $o] :
          ( ? [Xx: $i] :
              ( ( Xphi @ Xx )
              & ! [Xy: $i] :
                  ( ( Xphi @ Xy )
                 => ( Xx = Xy ) ) )
         => ( exu
            @ ^ [Xx: $i] :
                ( Xphi @ Xx ) ) ) ) )).

thf(exuI2,conjecture,
    ( exuI1
   => ! [Xphi: $i > $o] :
        ( ? [Xx: $i] :
          ! [Xy: $i] :
            ( ( Xphi @ Xy )
          <=> ( Xy = Xx ) )
       => ( exu
          @ ^ [Xx: $i] :
              ( Xphi @ Xx ) ) ) )).

%------------------------------------------------------------------------------
