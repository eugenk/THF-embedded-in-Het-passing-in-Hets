logic THF

spec SEU6942 =
%------------------------------------------------------------------------------
% File     : SEU694^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Functions - Extensionality and Beta Reduction
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! B:i.! f:i.func A B f -> (! x:i.in x A ->
%            ap A B f x = ap A B f x))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC196l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   30 (   1 equality;  13 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :   15 (   0   ~;   0   |;   0   &;  13   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    9 (   9   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :    4 (   0 sgn;   4   !;   0   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=358
%          : 
%------------------------------------------------------------------------------
thf(in,type,(
    in: $i > $i > $o )).

thf(func,type,(
    func: $i > $i > $i > $o )).

thf(ap,type,(
    ap: $i > $i > $i > $i > $i )).

thf(ap2apEq2,conjecture,(
    ! [A: $i,B: $i,Xf: $i] :
      ( ( func @ A @ B @ Xf )
     => ! [Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ( ap @ A @ B @ Xf @ Xx )
            = ( ap @ A @ B @ Xf @ Xx ) ) ) ) )).

%------------------------------------------------------------------------------
