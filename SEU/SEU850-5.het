logic THF

spec SEU8505 =
%------------------------------------------------------------------------------
% File     : SEU850^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory
% Problem  : TPS problem GAZING-THM9
% Version  : Especial.
% English  :

% Refs     : [Bar92] Barker-Plummer D (1992), Gazing: An Approach to the Pr
%          : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0358 [Bro09]
%          : 9 [Bar92]
%          : GAZING-THM9 [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   19 (   2 equality;  16 variable)
%            Maximal formula depth :   10 (   6 average)
%            Number of connectives :   13 (   0   ~;   0   |;   3   &;   6   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :    6 (   0 sgn;   6   !;   0   ?;   0   ^)
%                                         (   6   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cGAZING_THM9_pme,conjecture,(
    ! [S: a > $o,T: a > $o,U: a > $o] :
      ( ( ( S = T )
        & ( T = U ) )
     => ( ! [Xx: a] :
            ( ( S @ Xx )
           => ( T @ Xx ) )
        & ! [Xx: a] :
            ( ( T @ Xx )
           => ( U @ Xx ) )
        & ! [Xx: a] :
            ( ( U @ Xx )
           => ( S @ Xx ) ) ) ) )).

%------------------------------------------------------------------------------
