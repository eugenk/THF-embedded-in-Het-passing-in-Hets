logic THF

spec SEU9595 =
%------------------------------------------------------------------------------
% File     : SEU959^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Functions)
% Problem  : TPS problem from FUNCTION-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0916 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   26 (   3 equality;  13 variable)
%            Maximal formula depth :    8 (   5 average)
%            Number of connectives :   15 (   0   ~;   0   |;   0   &;  11   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :)
%            Number of variables   :    9 (   0 sgn;   5   !;   3   ?;   1   ^)
%                                         (   9   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(f,type,(
    f: $i > $i )).

thf(g,type,(
    g: $i > $i )).

thf(cTHM588AC2_pme,conjecture,
    ( ! [Xr: $i > $i > $o] :
        ( ! [Xx: $i] :
          ? [Xy: $i] :
            ( Xr @ Xx @ Xy )
       => ? [Xh: $i > $i] :
          ! [Xx: $i] :
            ( Xr @ Xx @ ( Xh @ Xx ) ) )
   => ( ! [Xx: $i,Xy: $i] :
          ( ( ( g @ Xx )
            = ( g @ Xy ) )
         => ( ( f @ Xx )
            = ( f @ Xy ) ) )
     => ? [Xh: $i > $i] :
          ( ( ^ [Xx: $i] :
                ( Xh @ ( g @ Xx ) ) )
          = f ) ) )).

%------------------------------------------------------------------------------
