logic THF

spec SEU8162 =
%------------------------------------------------------------------------------
% File     : SEU816^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Ordinals
% Version  : Especial > Reduced > Especial.
% English  : (! X:i.ordinal X -> (! Y:i.ordinal Y ->
%            transitiveset (binintersect X Y)))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC318l [Bro08]

% Status   : Theorem
% Rating   : 0.14 v6.0.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v3.7.0
% Syntax   : Number of formulae    :   18 (   2 unit;  11 type;   6 defn)
%            Number of atoms       :  124 (   9 equality;  53 variable)
%            Maximal formula depth :   14 (   5 average)
%            Number of connectives :   78 (   2   ~;   3   |;   6   &;  51   @)
%                                         (   0 <=>;  16  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   12 (  12   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   14 (  11   :)
%            Number of variables   :   19 (   0 sgn;  13   !;   1   ?;   5   ^)
%                                         (  19   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=512
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(emptyset_type,type,(
    emptyset: $i )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(nonempty_type,type,(
    nonempty: $i > $o )).

thf(nonempty,definition,
    ( nonempty
    = ( ^ [Xx: $i] : ( Xx != emptyset ) ) )).

thf(subset_type,type,(
    subset: $i > $i > $o )).

thf(binintersect_type,type,(
    binintersect: $i > $i > $i )).

thf(transitiveset_type,type,(
    transitiveset: $i > $o )).

thf(transitiveset,definition,
    ( transitiveset
    = ( ^ [A: $i] :
        ! [X: $i] :
          ( ( in @ X @ A )
         => ( subset @ X @ A ) ) ) )).

thf(binintTransitive_type,type,(
    binintTransitive: $o )).

thf(binintTransitive,definition,
    ( binintTransitive
    = ( ! [X: $i] :
          ( ( transitiveset @ X )
         => ! [Y: $i] :
              ( ( transitiveset @ Y )
             => ( transitiveset @ ( binintersect @ X @ Y ) ) ) ) ) )).

thf(stricttotalorderedByIn_type,type,(
    stricttotalorderedByIn: $i > $o )).

thf(stricttotalorderedByIn,definition,
    ( stricttotalorderedByIn
    = ( ^ [A: $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ! [X: $i] :
                  ( ( in @ X @ A )
                 => ! [Y: $i] :
                      ( ( in @ Y @ A )
                     => ( ( ( in @ Xx @ X )
                          & ( in @ X @ Y ) )
                       => ( in @ Xx @ Y ) ) ) ) )
          & ! [X: $i] :
              ( ( in @ X @ A )
             => ! [Y: $i] :
                  ( ( in @ Y @ A )
                 => ( ( X = Y )
                    | ( in @ X @ Y )
                    | ( in @ Y @ X ) ) ) )
          & ! [X: $i] :
              ( ( in @ X @ A )
             => ~ ( in @ X @ X ) ) ) ) )).

thf(wellorderedByIn_type,type,(
    wellorderedByIn: $i > $o )).

thf(wellorderedByIn,definition,
    ( wellorderedByIn
    = ( ^ [A: $i] :
          ( ( stricttotalorderedByIn @ A )
          & ! [X: $i] :
              ( ( in @ X @ ( powerset @ A ) )
             => ( ( nonempty @ X )
               => ? [Xx: $i] :
                    ( ( in @ Xx @ X )
                    & ! [Y: $i] :
                        ( ( in @ Y @ X )
                       => ( ( Xx = Y )
                          | ( in @ Xx @ Y ) ) ) ) ) ) ) ) )).

thf(ordinal_type,type,(
    ordinal: $i > $o )).

thf(ordinal,definition,
    ( ordinal
    = ( ^ [Xx: $i] :
          ( ( transitiveset @ Xx )
          & ( wellorderedByIn @ Xx ) ) ) )).

thf(ordinalMinLem1,conjecture,
    ( binintTransitive
   => ! [X: $i] :
        ( ( ordinal @ X )
       => ! [Y: $i] :
            ( ( ordinal @ Y )
           => ( transitiveset @ ( binintersect @ X @ Y ) ) ) ) )).

%------------------------------------------------------------------------------
