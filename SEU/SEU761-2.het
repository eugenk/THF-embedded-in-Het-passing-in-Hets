logic THF

spec SEU7612 =
%------------------------------------------------------------------------------
% File     : SEU761^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - First Wizard of Oz Examples - WoZ1 Lemmas
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> (! Y:i.in Y (powerset A) ->
%            (! Z:i.in Z (powerset A) -> subset X Y -> subset X Z ->
%            subset X (binintersect Y Z))))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC263l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :   11 (   3 unit;   7 type;   3 defn)
%            Number of atoms       :   75 (   3 equality;  32 variable)
%            Maximal formula depth :   16 (   6 average)
%            Number of connectives :   51 (   0   ~;   0   |;   0   &;  37   @)
%                                         (   0 <=>;  14  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    7 (   7   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    9 (   7   :)
%            Number of variables   :   13 (   0 sgn;  13   !;   0   ?;   0   ^)
%                                         (  13   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=322
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(subset_type,type,(
    subset: $i > $i > $o )).

thf(subsetI1_type,type,(
    subsetI1: $o )).

thf(subsetI1,definition,
    ( subsetI1
    = ( ! [A: $i,B: $i] :
          ( ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( in @ Xx @ B ) )
         => ( subset @ A @ B ) ) ) )).

thf(subsetE_type,type,(
    subsetE: $o )).

thf(subsetE,definition,
    ( subsetE
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( subset @ A @ B )
         => ( ( in @ Xx @ A )
           => ( in @ Xx @ B ) ) ) ) )).

thf(binintersect_type,type,(
    binintersect: $i > $i > $i )).

thf(binintersectI_type,type,(
    binintersectI: $o )).

thf(binintersectI,definition,
    ( binintersectI
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ( in @ Xx @ B )
           => ( in @ Xx @ ( binintersect @ A @ B ) ) ) ) ) )).

thf(woz13rule3,conjecture,
    ( subsetI1
   => ( subsetE
     => ( binintersectI
       => ! [A: $i,X: $i] :
            ( ( in @ X @ ( powerset @ A ) )
           => ! [Y: $i] :
                ( ( in @ Y @ ( powerset @ A ) )
               => ! [Z: $i] :
                    ( ( in @ Z @ ( powerset @ A ) )
                   => ( ( subset @ X @ Y )
                     => ( ( subset @ X @ Z )
                       => ( subset @ X @ ( binintersect @ Y @ Z ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
