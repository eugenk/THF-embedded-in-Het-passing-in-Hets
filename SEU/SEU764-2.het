logic THF

spec SEU7642 =
%------------------------------------------------------------------------------
% File     : SEU764^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - First Wizard of Oz Examples - WoZ1 Problems
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> (! Y:i.in Y (powerset A) ->
%            (! Z:i.in Z (powerset A) -> (! W:i.in W (powerset A) ->
%            setminus A (binintersect (binunion X Y) (binunion Z W)) =
%            binunion (binintersect (setminus A X) (setminus A Y))
%            (binintersect (setminus A Z) (setminus A W))))))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC266l [Bro08]

% Status   : Theorem
% Rating   : 0.29 v6.1.0, 0.43 v5.5.0, 0.33 v5.4.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.40 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   12 (   3 unit;   8 type;   3 defn)
%            Number of atoms       :  123 (   6 equality;  50 variable)
%            Maximal formula depth :   20 (   7 average)
%            Number of connectives :   90 (   0   ~;   0   |;   0   &;  77   @)
%                                         (   0 <=>;  13  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    9 (   9   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   10 (   8   :)
%            Number of variables   :   14 (   0 sgn;  14   !;   0   ?;   0   ^)
%                                         (  14   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=325
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(binunion_type,type,(
    binunion: $i > $i > $i )).

thf(binintersect_type,type,(
    binintersect: $i > $i > $i )).

thf(setminus_type,type,(
    setminus: $i > $i > $i )).

thf(binunionT_lem_type,type,(
    binunionT_lem: $o )).

thf(binunionT_lem,definition,
    ( binunionT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( in @ ( binunion @ X @ Y ) @ ( powerset @ A ) ) ) ) ) )).

thf(demorgan1_type,type,(
    demorgan1: $o )).

thf(demorgan1,definition,
    ( demorgan1
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( ( setminus @ A @ ( binintersect @ X @ Y ) )
                = ( binunion @ ( setminus @ A @ X ) @ ( setminus @ A @ Y ) ) ) ) ) ) )).

thf(demorgan2_type,type,(
    demorgan2: $o )).

thf(demorgan2,definition,
    ( demorgan2
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( ( setminus @ A @ ( binunion @ X @ Y ) )
                = ( binintersect @ ( setminus @ A @ X ) @ ( setminus @ A @ Y ) ) ) ) ) ) )).

thf(woz1_2,conjecture,
    ( binunionT_lem
   => ( demorgan1
     => ( demorgan2
       => ! [A: $i,X: $i] :
            ( ( in @ X @ ( powerset @ A ) )
           => ! [Y: $i] :
                ( ( in @ Y @ ( powerset @ A ) )
               => ! [Z: $i] :
                    ( ( in @ Z @ ( powerset @ A ) )
                   => ! [W: $i] :
                        ( ( in @ W @ ( powerset @ A ) )
                       => ( ( setminus @ A @ ( binintersect @ ( binunion @ X @ Y ) @ ( binunion @ Z @ W ) ) )
                          = ( binunion @ ( binintersect @ ( setminus @ A @ X ) @ ( setminus @ A @ Y ) ) @ ( binintersect @ ( setminus @ A @ Z ) @ ( setminus @ A @ W ) ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
