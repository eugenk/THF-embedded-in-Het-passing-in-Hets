logic THF

spec SEU9305 =
%------------------------------------------------------------------------------
% File     : SEU930^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Functions)
% Problem  : TPS problem THM171A
% Version  : Especial.
% English  : If g commutes with f, any unique fixed point of gi is a fixed 
%            point of f.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0250 [Bro09]
%          : THM171A [TPS]

% Status   : Theorem
% Rating   : 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   26 (   5 equality;  10 variable)
%            Maximal formula depth :    9 (   5 average)
%            Number of connectives :   11 (   0   ~;   0   |;   1   &;   7   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   2   :)
%            Number of variables   :    4 (   0 sgn;   2   !;   0   ?;   2   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(f,type,(
    f: $i > $i )).

thf(g,type,(
    g: $i > $i )).

thf(cTHM171A_pme,conjecture,
    ( ( ( ^ [Xx: $i] :
            ( f @ ( g @ Xx ) ) )
      = ( ^ [Xx: $i] :
            ( g @ ( f @ Xx ) ) ) )
   => ! [Xx: $i] :
        ( ( ( ( g @ Xx )
            = Xx )
          & ! [Xz: $i] :
              ( ( ( g @ Xz )
                = Xz )
             => ( Xz = Xx ) ) )
       => ( ( f @ Xx )
          = Xx ) ) )).

%------------------------------------------------------------------------------
