logic THF

spec SEU6112 =
%------------------------------------------------------------------------------
% File     : SEU611^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Preliminary Notions - Operations on Sets - Symmetric Difference
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! B:i.! x:i.in x (symdiff A B) -> (! phi:o.(in x A ->
%            ~(in x B) -> phi) -> (~(in x A) -> in x B -> phi) -> phi))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC113l [Bro08]

% Status   : Theorem
% Rating   : 0.57 v5.5.0, 0.50 v5.4.0, 0.60 v4.1.0, 0.67 v4.0.1, 1.00 v4.0.0, 0.67 v3.7.0
% Syntax   : Number of formulae    :   12 (   3 unit;   7 type;   4 defn)
%            Number of atoms       :   85 (   4 equality;  39 variable)
%            Maximal formula depth :   15 (   6 average)
%            Number of connectives :   60 (   4   ~;   2   |;   0   &;  41   @)
%                                         (   0 <=>;  13  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   11 (  11   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   10 (   7   :)
%            Number of variables   :   18 (   0 sgn;  13   !;   0   ?;   5   ^)
%                                         (  18   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=489
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(dsetconstr_type,type,(
    dsetconstr: $i > ( $i > $o ) > $i )).

thf(dsetconstrEL_type,type,(
    dsetconstrEL: $o )).

thf(dsetconstrEL,definition,
    ( dsetconstrEL
    = ( ! [A: $i,Xphi: $i > $o,Xx: $i] :
          ( ( in @ Xx
            @ ( dsetconstr @ A
              @ ^ [Xy: $i] :
                  ( Xphi @ Xy ) ) )
         => ( in @ Xx @ A ) ) ) )).

thf(dsetconstrER_type,type,(
    dsetconstrER: $o )).

thf(dsetconstrER,definition,
    ( dsetconstrER
    = ( ! [A: $i,Xphi: $i > $o,Xx: $i] :
          ( ( in @ Xx
            @ ( dsetconstr @ A
              @ ^ [Xy: $i] :
                  ( Xphi @ Xy ) ) )
         => ( Xphi @ Xx ) ) ) )).

thf(binunion_type,type,(
    binunion: $i > $i > $i )).

thf(binunionE_type,type,(
    binunionE: $o )).

thf(binunionE,definition,
    ( binunionE
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ ( binunion @ A @ B ) )
         => ( ( in @ Xx @ A )
            | ( in @ Xx @ B ) ) ) ) )).

thf(symdiff_type,type,(
    symdiff: $i > $i > $i )).

thf(symdiff,definition,
    ( symdiff
    = ( ^ [A: $i,B: $i] :
          ( dsetconstr @ ( binunion @ A @ B )
          @ ^ [Xx: $i] :
              ( ~ ( in @ Xx @ A )
              | ~ ( in @ Xx @ B ) ) ) ) )).

thf(symdiffE,conjecture,
    ( dsetconstrEL
   => ( dsetconstrER
     => ( binunionE
       => ! [A: $i,B: $i,Xx: $i] :
            ( ( in @ Xx @ ( symdiff @ A @ B ) )
           => ! [Xphi: $o] :
                ( ( ( in @ Xx @ A )
                 => ( ~ ( in @ Xx @ B )
                   => Xphi ) )
               => ( ( ~ ( in @ Xx @ A )
                   => ( ( in @ Xx @ B )
                     => Xphi ) )
                 => Xphi ) ) ) ) ) )).

%------------------------------------------------------------------------------
