logic THF

spec SEU6502 =
%------------------------------------------------------------------------------
% File     : SEU650^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Ordered Pairs - Properties of Pairs
% Version  : Especial > Reduced > Especial.
% English  : (! x:i.! y:i.x = y -> setadjoin (setadjoin x emptyset) (setadjoin
%            (setadjoin x (setadjoin y emptyset)) emptyset) = setadjoin
%            (setadjoin x emptyset) emptyset)

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC152l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.1.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v4.0.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :    5 (   2 unit;   3 type;   1 defn)
%            Number of atoms       :   40 (   5 equality;  11 variable)
%            Maximal formula depth :   12 (   6 average)
%            Number of connectives :   23 (   0   ~;   0   |;   0   &;  20   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :    4 (   0 sgn;   4   !;   0   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=206
%          : 
%------------------------------------------------------------------------------
thf(emptyset_type,type,(
    emptyset: $i )).

thf(setadjoin_type,type,(
    setadjoin: $i > $i > $i )).

thf(setukpairinjR11_type,type,(
    setukpairinjR11: $o )).

thf(setukpairinjR11,definition,
    ( setukpairinjR11
    = ( ! [Xx: $i,Xy: $i] :
          ( ( Xx = Xy )
         => ( ( setadjoin @ Xx @ ( setadjoin @ Xy @ emptyset ) )
            = ( setadjoin @ Xx @ emptyset ) ) ) ) )).

thf(setukpairinjR12,conjecture,
    ( setukpairinjR11
   => ! [Xx: $i,Xy: $i] :
        ( ( Xx = Xy )
       => ( ( setadjoin @ ( setadjoin @ Xx @ emptyset ) @ ( setadjoin @ ( setadjoin @ Xx @ ( setadjoin @ Xy @ emptyset ) ) @ emptyset ) )
          = ( setadjoin @ ( setadjoin @ Xx @ emptyset ) @ emptyset ) ) ) )).

%------------------------------------------------------------------------------
