logic THF

spec SEU7312 =
%------------------------------------------------------------------------------
% File     : SEU731^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - Laws for Typed Sets
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> X = setminus A (setminus A X))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC233l [Bro08]

% Status   : Theorem
% Rating   : 0.14 v6.1.0, 0.29 v5.5.0, 0.33 v5.4.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.40 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   12 (   4 unit;   7 type;   4 defn)
%            Number of atoms       :  108 (   6 equality;  49 variable)
%            Maximal formula depth :   14 (   6 average)
%            Number of connectives :   79 (   0   ~;   0   |;   0   &;  59   @)
%                                         (   0 <=>;  20  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    9 (   7   :)
%            Number of variables   :   15 (   0 sgn;  15   !;   0   ?;   0   ^)
%                                         (  15   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=291
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(setminus_type,type,(
    setminus: $i > $i > $i )).

thf(complementT_lem_type,type,(
    complementT_lem: $o )).

thf(complementT_lem,definition,
    ( complementT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ( in @ ( setminus @ A @ X ) @ ( powerset @ A ) ) ) ) )).

thf(setextT_type,type,(
    setextT: $o )).

thf(setextT,definition,
    ( setextT
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ( ( in @ Xx @ X )
                     => ( in @ Xx @ Y ) ) )
               => ( ! [Xx: $i] :
                      ( ( in @ Xx @ A )
                     => ( ( in @ Xx @ Y )
                       => ( in @ Xx @ X ) ) )
                 => ( X = Y ) ) ) ) ) ) )).

thf(doubleComplementI1_type,type,(
    doubleComplementI1: $o )).

thf(doubleComplementI1,definition,
    ( doubleComplementI1
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( ( in @ Xx @ X )
               => ( in @ Xx @ ( setminus @ A @ ( setminus @ A @ X ) ) ) ) ) ) ) )).

thf(doubleComplementE1_type,type,(
    doubleComplementE1: $o )).

thf(doubleComplementE1,definition,
    ( doubleComplementE1
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Xx: $i] :
              ( ( in @ Xx @ A )
             => ( ( in @ Xx @ ( setminus @ A @ ( setminus @ A @ X ) ) )
               => ( in @ Xx @ X ) ) ) ) ) )).

thf(doubleComplementEq,conjecture,
    ( complementT_lem
   => ( setextT
     => ( doubleComplementI1
       => ( doubleComplementE1
         => ! [A: $i,X: $i] :
              ( ( in @ X @ ( powerset @ A ) )
             => ( X
                = ( setminus @ A @ ( setminus @ A @ X ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
