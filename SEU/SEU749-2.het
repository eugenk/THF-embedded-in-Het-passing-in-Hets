logic THF

spec SEU7492 =
%------------------------------------------------------------------------------
% File     : SEU749^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - Laws for Typed Sets - DeMorgan Laws
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> (! Y:i.in Y (powerset A) ->
%            in (setminus A (binunion X Y)) (powerset (setminus A X))))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC251l [Bro08]
%          : ZFC269l [Bro08]

% Status   : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.33 v5.4.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.40 v4.1.0, 0.33 v4.0.1, 0.67 v4.0.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   13 (   4 unit;   8 type;   4 defn)
%            Number of atoms       :  113 (   4 equality;  46 variable)
%            Maximal formula depth :   15 (   7 average)
%            Number of connectives :   85 (   0   ~;   0   |;   0   &;  67   @)
%                                         (   0 <=>;  18  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    7 (   7   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   10 (   8   :)
%            Number of variables   :   16 (   0 sgn;  16   !;   0   ?;   0   ^)
%                                         (  16   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=379
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(binunion_type,type,(
    binunion: $i > $i > $i )).

thf(setminus_type,type,(
    setminus: $i > $i > $i )).

thf(binunionT_lem_type,type,(
    binunionT_lem: $o )).

thf(binunionT_lem,definition,
    ( binunionT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( in @ ( binunion @ X @ Y ) @ ( powerset @ A ) ) ) ) ) )).

thf(complementT_lem_type,type,(
    complementT_lem: $o )).

thf(complementT_lem,definition,
    ( complementT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ( in @ ( setminus @ A @ X ) @ ( powerset @ A ) ) ) ) )).

thf(powersetTI1_type,type,(
    powersetTI1: $o )).

thf(powersetTI1,definition,
    ( powersetTI1
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ( ( in @ Xx @ X )
                     => ( in @ Xx @ Y ) ) )
               => ( in @ X @ ( powerset @ Y ) ) ) ) ) ) )).

thf(demorgan2a1_type,type,(
    demorgan2a1: $o )).

thf(demorgan2a1,definition,
    ( demorgan2a1
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ( ( in @ Xx @ ( setminus @ A @ ( binunion @ X @ Y ) ) )
                   => ( in @ Xx @ ( setminus @ A @ X ) ) ) ) ) ) ) )).

thf(complementUnionInPowersetComplement,conjecture,
    ( binunionT_lem
   => ( complementT_lem
     => ( powersetTI1
       => ( demorgan2a1
         => ! [A: $i,X: $i] :
              ( ( in @ X @ ( powerset @ A ) )
             => ! [Y: $i] :
                  ( ( in @ Y @ ( powerset @ A ) )
                 => ( in @ ( setminus @ A @ ( binunion @ X @ Y ) ) @ ( powerset @ ( setminus @ A @ X ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
