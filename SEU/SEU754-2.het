logic THF

spec SEU7542 =
%------------------------------------------------------------------------------
% File     : SEU754^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - Laws for Typed Sets - DeMorgan Laws
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> (! Y:i.in Y (powerset A) ->
%            (! x:i.in x A -> in x (setminus A (binunion X Y)) ->
%            in x (binintersect (setminus A X) (setminus A Y)))))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC256l [Bro08]

% Status   : Theorem
% Rating   : 0.29 v6.1.0, 0.43 v5.5.0, 0.33 v5.4.0, 0.40 v5.1.0, 0.60 v5.0.0, 0.40 v4.1.0, 0.33 v4.0.1, 0.67 v4.0.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   16 (   5 unit;  10 type;   5 defn)
%            Number of atoms       :  129 (   5 equality;  56 variable)
%            Maximal formula depth :   19 (   7 average)
%            Number of connectives :  100 (   6   ~;   0   |;   0   &;  72   @)
%                                         (   0 <=>;  22  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    9 (   9   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   13 (  10   :)
%            Number of variables   :   21 (   0 sgn;  21   !;   0   ?;   0   ^)
%                                         (  21   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=315
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(binunion_type,type,(
    binunion: $i > $i > $i )).

thf(binintersect_type,type,(
    binintersect: $i > $i > $i )).

thf(binintersectI_type,type,(
    binintersectI: $o )).

thf(binintersectI,definition,
    ( binintersectI
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ( in @ Xx @ B )
           => ( in @ Xx @ ( binintersect @ A @ B ) ) ) ) ) )).

thf(setminus_type,type,(
    setminus: $i > $i > $i )).

thf(setminusI_type,type,(
    setminusI: $o )).

thf(setminusI,definition,
    ( setminusI
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ~ ( in @ Xx @ B )
           => ( in @ Xx @ ( setminus @ A @ B ) ) ) ) ) )).

thf(setminusER_type,type,(
    setminusER: $o )).

thf(setminusER,definition,
    ( setminusER
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ ( setminus @ A @ B ) )
         => ~ ( in @ Xx @ B ) ) ) )).

thf(binunionTILcontra_type,type,(
    binunionTILcontra: $o )).

thf(binunionTILcontra,definition,
    ( binunionTILcontra
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ( ~ ( in @ Xx @ ( binunion @ X @ Y ) )
                   => ~ ( in @ Xx @ X ) ) ) ) ) ) )).

thf(binunionTIRcontra_type,type,(
    binunionTIRcontra: $o )).

thf(binunionTIRcontra,definition,
    ( binunionTIRcontra
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ( ~ ( in @ Xx @ ( binunion @ X @ Y ) )
                   => ~ ( in @ Xx @ Y ) ) ) ) ) ) )).

thf(demorgan2a,conjecture,
    ( binintersectI
   => ( setminusI
     => ( setminusER
       => ( binunionTILcontra
         => ( binunionTIRcontra
           => ! [A: $i,X: $i] :
                ( ( in @ X @ ( powerset @ A ) )
               => ! [Y: $i] :
                    ( ( in @ Y @ ( powerset @ A ) )
                   => ! [Xx: $i] :
                        ( ( in @ Xx @ A )
                       => ( ( in @ Xx @ ( setminus @ A @ ( binunion @ X @ Y ) ) )
                         => ( in @ Xx @ ( binintersect @ ( setminus @ A @ X ) @ ( setminus @ A @ Y ) ) ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
