logic THF

spec SEU7572 =
%------------------------------------------------------------------------------
% File     : SEU757^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - Laws for Typed Sets - DeMorgan Laws
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> (! Y:i.in Y (powerset A) ->
%            setminus A (binunion X Y) = binintersect (setminus A X)
%            (setminus A Y)))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC259l [Bro08]

% Status   : Theorem
% Rating   : 0.43 v6.1.0, 0.57 v5.5.0, 0.50 v5.4.0, 0.60 v4.1.0, 0.67 v4.0.1, 1.00 v4.0.0, 0.67 v3.7.0
% Syntax   : Number of formulae    :   18 (   6 unit;  11 type;   6 defn)
%            Number of atoms       :  180 (   8 equality;  78 variable)
%            Maximal formula depth :   17 (   7 average)
%            Number of connectives :  137 (   0   ~;   0   |;   0   &; 108   @)
%                                         (   0 <=>;  29  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    9 (   9   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   13 (  11   :)
%            Number of variables   :   24 (   0 sgn;  24   !;   0   ?;   0   ^)
%                                         (  24   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=318
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(binunion_type,type,(
    binunion: $i > $i > $i )).

thf(binintersect_type,type,(
    binintersect: $i > $i > $i )).

thf(setminus_type,type,(
    setminus: $i > $i > $i )).

thf(binintersectT_lem_type,type,(
    binintersectT_lem: $o )).

thf(binintersectT_lem,definition,
    ( binintersectT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( in @ ( binintersect @ X @ Y ) @ ( powerset @ A ) ) ) ) ) )).

thf(binunionT_lem_type,type,(
    binunionT_lem: $o )).

thf(binunionT_lem,definition,
    ( binunionT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( in @ ( binunion @ X @ Y ) @ ( powerset @ A ) ) ) ) ) )).

thf(complementT_lem_type,type,(
    complementT_lem: $o )).

thf(complementT_lem,definition,
    ( complementT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ( in @ ( setminus @ A @ X ) @ ( powerset @ A ) ) ) ) )).

thf(setextT_type,type,(
    setextT: $o )).

thf(setextT,definition,
    ( setextT
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ( ( in @ Xx @ X )
                     => ( in @ Xx @ Y ) ) )
               => ( ! [Xx: $i] :
                      ( ( in @ Xx @ A )
                     => ( ( in @ Xx @ Y )
                       => ( in @ Xx @ X ) ) )
                 => ( X = Y ) ) ) ) ) ) )).

thf(demorgan2a_type,type,(
    demorgan2a: $o )).

thf(demorgan2a,definition,
    ( demorgan2a
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ( ( in @ Xx @ ( setminus @ A @ ( binunion @ X @ Y ) ) )
                   => ( in @ Xx @ ( binintersect @ ( setminus @ A @ X ) @ ( setminus @ A @ Y ) ) ) ) ) ) ) ) )).

thf(demorgan2b_type,type,(
    demorgan2b: $o )).

thf(demorgan2b,definition,
    ( demorgan2b
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ! [Xx: $i] :
                  ( ( in @ Xx @ A )
                 => ( ( in @ Xx @ ( binintersect @ ( setminus @ A @ X ) @ ( setminus @ A @ Y ) ) )
                   => ( in @ Xx @ ( setminus @ A @ ( binunion @ X @ Y ) ) ) ) ) ) ) ) )).

thf(demorgan2,conjecture,
    ( binintersectT_lem
   => ( binunionT_lem
     => ( complementT_lem
       => ( setextT
         => ( demorgan2a
           => ( demorgan2b
             => ! [A: $i,X: $i] :
                  ( ( in @ X @ ( powerset @ A ) )
                 => ! [Y: $i] :
                      ( ( in @ Y @ ( powerset @ A ) )
                     => ( ( setminus @ A @ ( binunion @ X @ Y ) )
                        = ( binintersect @ ( setminus @ A @ X ) @ ( setminus @ A @ Y ) ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
