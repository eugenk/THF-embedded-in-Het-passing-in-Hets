logic THF

spec SEU7252 =
%------------------------------------------------------------------------------
% File     : SEU725^2 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Set Theory
% Problem  : Typed Set Theory - Laws for Typed Sets
% Version  : Especial > Reduced > Especial.
% English  : (! A:i.! X:i.in X (powerset A) -> (! Y:i.in Y (powerset A) ->
%            subset X Y -> subset (setminus A Y) (setminus A X)))

% Refs     : [Bro08] Brown (2008), Email to G. Sutcliffe
% Source   : [Bro08]
% Names    : ZFC227l [Bro08]

% Status   : Theorem
% Rating   : 0.14 v6.0.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.40 v5.2.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.33 v4.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :   15 (   5 unit;   9 type;   5 defn)
%            Number of atoms       :  107 (   5 equality;  45 variable)
%            Maximal formula depth :   16 (   6 average)
%            Number of connectives :   77 (   2   ~;   0   |;   0   &;  56   @)
%                                         (   0 <=>;  19  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    7 (   7   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   12 (   9   :)
%            Number of variables   :   18 (   0 sgn;  18   !;   0   ?;   0   ^)
%                                         (  18   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : http://mathgate.info/detsetitem.php?id=285
%          : 
%------------------------------------------------------------------------------
thf(in_type,type,(
    in: $i > $i > $o )).

thf(powerset_type,type,(
    powerset: $i > $i )).

thf(subset_type,type,(
    subset: $i > $i > $o )).

thf(subsetE_type,type,(
    subsetE: $o )).

thf(subsetE,definition,
    ( subsetE
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( subset @ A @ B )
         => ( ( in @ Xx @ A )
           => ( in @ Xx @ B ) ) ) ) )).

thf(setminus_type,type,(
    setminus: $i > $i > $i )).

thf(setminusI_type,type,(
    setminusI: $o )).

thf(setminusI,definition,
    ( setminusI
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ A )
         => ( ~ ( in @ Xx @ B )
           => ( in @ Xx @ ( setminus @ A @ B ) ) ) ) ) )).

thf(setminusER_type,type,(
    setminusER: $o )).

thf(setminusER,definition,
    ( setminusER
    = ( ! [A: $i,B: $i,Xx: $i] :
          ( ( in @ Xx @ ( setminus @ A @ B ) )
         => ~ ( in @ Xx @ B ) ) ) )).

thf(complementT_lem_type,type,(
    complementT_lem: $o )).

thf(complementT_lem,definition,
    ( complementT_lem
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ( in @ ( setminus @ A @ X ) @ ( powerset @ A ) ) ) ) )).

thf(subsetTI_type,type,(
    subsetTI: $o )).

thf(subsetTI,definition,
    ( subsetTI
    = ( ! [A: $i,X: $i] :
          ( ( in @ X @ ( powerset @ A ) )
         => ! [Y: $i] :
              ( ( in @ Y @ ( powerset @ A ) )
             => ( ! [Xx: $i] :
                    ( ( in @ Xx @ A )
                   => ( ( in @ Xx @ X )
                     => ( in @ Xx @ Y ) ) )
               => ( subset @ X @ Y ) ) ) ) ) )).

thf(contrasubsetT2,conjecture,
    ( subsetE
   => ( setminusI
     => ( setminusER
       => ( complementT_lem
         => ( subsetTI
           => ! [A: $i,X: $i] :
                ( ( in @ X @ ( powerset @ A ) )
               => ! [Y: $i] :
                    ( ( in @ Y @ ( powerset @ A ) )
                   => ( ( subset @ X @ Y )
                     => ( subset @ ( setminus @ A @ Y ) @ ( setminus @ A @ X ) ) ) ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
