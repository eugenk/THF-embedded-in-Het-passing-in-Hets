logic THF

spec CSR1423 =
%------------------------------------------------------------------------------
% File     : CSR142^3 : TPTP v6.2.0. Released v4.1.0.
% Domain   : Commonsense Reasoning
% Problem  : Who is the husband of Corina?
% Version  : Especial.
% English  : Corina is the wife of Chris. Who is the husband of Corina?

% Refs     : [Ben10] Benzmueller (2010), Email to Geoff Sutcliffe
% Source   : [Ben10]
% Names    : ex_1.tq_SUMO_local [Ben10]

% Status   : CounterSatisfiable
% Rating   : 0.67 v6.1.0, 0.83 v6.0.0, 0.67 v5.5.0, 0.80 v5.4.0, 0.75 v4.1.0
% Syntax   : Number of formulae    :    7 (   3 unit;   5 type;   0 defn)
%            Number of atoms       :   15 (   0 equality;   1 variable)
%            Maximal formula depth :    4 (   3 average)
%            Number of connectives :    4 (   0   ~;   0   |;   0   &;   4   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    8 (   5   :)
%            Number of variables   :    1 (   0 sgn;   0   !;   1   ?;   0   ^)
%                                         (   1   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This is a simple test problem for reasoning in/about SUMO.
%            Initally the problem has been hand generated in KIF syntax in
%            SigmaKEE and then automatically translated by Benzmueller's
%            KIF2TH0 translator into THF syntax.
%          : The translation has been applied in three modes: handselected,
%            SInE, and local. The local mode only translates the local
%            assumptions and the query. The SInE mode additionally translates
%            the SInE extract of the loaded knowledge base (usually SUMO). The
%            handselected mode contains a hand-selected relevant axioms.
%          : The examples are selected to illustrate the benefits of
%            higher-order reasoning in ontology reasoning.
%------------------------------------------------------------------------------
%----The extracted Signature
thf(numbers,type,(
    num: $tType )).

thf(husband_THFTYPE_IiioI,type,(
    husband_THFTYPE_IiioI: $i > $i > $o )).

thf(lChris_THFTYPE_i,type,(
    lChris_THFTYPE_i: $i )).

thf(lCorina_THFTYPE_i,type,(
    lCorina_THFTYPE_i: $i )).

thf(wife_THFTYPE_IiioI,type,(
    wife_THFTYPE_IiioI: $i > $i > $o )).

%----The translated axioms
thf(ax,axiom,
    ( wife_THFTYPE_IiioI @ lCorina_THFTYPE_i @ lChris_THFTYPE_i )).

%----The translated conjectures
thf(con,conjecture,(
    ? [X: $i] :
      ( husband_THFTYPE_IiioI @ X @ lCorina_THFTYPE_i ) )).

%------------------------------------------------------------------------------
