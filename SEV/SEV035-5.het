logic THF

spec SEV0355 =
%------------------------------------------------------------------------------
% File     : SEV035^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from EQUIVALENCE-RELATIONS-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1195 [Bro09]
%          : tps_1196 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :  109 (  18 equality;  90 variable)
%            Maximal formula depth :   18 (  10 average)
%            Number of connectives :   71 (   0   ~;   0   |;  15   &;  42   @)
%                                         (   0 <=>;  14  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   12 (  12   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   48 (   0 sgn;  18   !;  12   ?;  18   ^)
%                                         (  48   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cEQP1_1_pme,conjecture,
    ( ! [Xx: a > $o] :
      ? [Xs: a > a] :
        ( ! [Xx0: a] :
            ( ( Xx @ Xx0 )
           => ( Xx @ ( Xs @ Xx0 ) ) )
        & ! [Xy: a] :
            ( ( Xx @ Xy )
           => ? [Xy_20: a] :
                ( ( ^ [Xx0: a] :
                      ( ( Xx @ Xx0 )
                      & ( Xy
                        = ( Xs @ Xx0 ) ) ) )
                = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                  @ Xy_20 ) ) ) )
    & ! [Xx: a > $o,Xy: a > $o] :
        ( ? [Xs: a > a] :
            ( ! [Xx0: a] :
                ( ( Xx @ Xx0 )
               => ( Xy @ ( Xs @ Xx0 ) ) )
            & ! [Xy0: a] :
                ( ( Xy @ Xy0 )
               => ? [Xy_21: a] :
                    ( ( ^ [Xx0: a] :
                          ( ( Xx @ Xx0 )
                          & ( Xy0
                            = ( Xs @ Xx0 ) ) ) )
                    = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                      @ Xy_21 ) ) ) )
       => ? [Xs: a > a] :
            ( ! [Xx0: a] :
                ( ( Xy @ Xx0 )
               => ( Xx @ ( Xs @ Xx0 ) ) )
            & ! [Xy0: a] :
                ( ( Xx @ Xy0 )
               => ? [Xy_22: a] :
                    ( ( ^ [Xx0: a] :
                          ( ( Xy @ Xx0 )
                          & ( Xy0
                            = ( Xs @ Xx0 ) ) ) )
                    = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                      @ Xy_22 ) ) ) ) )
    & ! [Xx: a > $o,Xy: a > $o,Xz: a > $o] :
        ( ( ? [Xs: a > a] :
              ( ! [Xx0: a] :
                  ( ( Xx @ Xx0 )
                 => ( Xy @ ( Xs @ Xx0 ) ) )
              & ! [Xy0: a] :
                  ( ( Xy @ Xy0 )
                 => ? [Xy_23: a] :
                      ( ( ^ [Xx0: a] :
                            ( ( Xx @ Xx0 )
                            & ( Xy0
                              = ( Xs @ Xx0 ) ) ) )
                      = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                        @ Xy_23 ) ) ) )
          & ? [Xs: a > a] :
              ( ! [Xx0: a] :
                  ( ( Xy @ Xx0 )
                 => ( Xz @ ( Xs @ Xx0 ) ) )
              & ! [Xy0: a] :
                  ( ( Xz @ Xy0 )
                 => ? [Xy_24: a] :
                      ( ( ^ [Xx0: a] :
                            ( ( Xy @ Xx0 )
                            & ( Xy0
                              = ( Xs @ Xx0 ) ) ) )
                      = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                        @ Xy_24 ) ) ) ) )
       => ? [Xs: a > a] :
            ( ! [Xx0: a] :
                ( ( Xx @ Xx0 )
               => ( Xz @ ( Xs @ Xx0 ) ) )
            & ! [Xy0: a] :
                ( ( Xz @ Xy0 )
               => ? [Xy_25: a] :
                    ( ( ^ [Xx0: a] :
                          ( ( Xx @ Xx0 )
                          & ( Xy0
                            = ( Xs @ Xx0 ) ) ) )
                    = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                      @ Xy_25 ) ) ) ) ) )).

%------------------------------------------------------------------------------
