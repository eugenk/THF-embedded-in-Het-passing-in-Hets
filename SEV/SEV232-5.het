logic THF

spec SEV2325 =
%------------------------------------------------------------------------------
% File     : SEV232^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem X6007
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0359 [Bro09]
%          : X6007 [TPS]

% Status   : Theorem
% Rating   : 0.14 v6.1.0, 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   28 (   1 equality;  14 variable)
%            Maximal formula depth :   10 (   6 average)
%            Number of connectives :   16 (   0   ~;   0   |;   2   &;  10   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   21 (  21   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :)
%            Number of variables   :    6 (   0 sgn;   4   !;   0   ?;   2   ^)
%                                         (   6   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(cS,type,(
    cS: ( ( $i > $o ) > $o ) > ( $i > $o ) > $o )).

thf(c0,type,(
    c0: ( $i > $o ) > $o )).

thf(cX6007_pme,conjecture,
    ( ( ^ [N: ( $i > $o ) > $o] :
        ! [P: ( ( $i > $o ) > $o ) > $o] :
          ( ( ( P @ c0 )
            & ! [X: ( $i > $o ) > $o] :
                ( ( P @ X )
               => ( P @ ( cS @ X ) ) ) )
         => ( P @ N ) ) )
    = ( ^ [Xx: ( $i > $o ) > $o] :
        ! [S0: ( ( $i > $o ) > $o ) > $o] :
          ( ( ( S0 @ c0 )
            & ! [X: ( $i > $o ) > $o] :
                ( ( S0 @ X )
               => ( S0 @ ( cS @ X ) ) ) )
         => ( S0 @ Xx ) ) ) )).

%------------------------------------------------------------------------------
