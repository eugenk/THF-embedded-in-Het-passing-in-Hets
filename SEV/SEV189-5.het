logic THF

spec SEV1895 =
%------------------------------------------------------------------------------
% File     : SEV189^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem from CLOS-SYS-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1039 [Bro09]

% Status   : Theorem
% Rating   : 0.33 v6.2.0, 0.17 v5.5.0, 0.20 v5.4.0, 0.25 v4.1.0, 0.33 v4.0.0
% Syntax   : Number of formulae    :    4 (   1 unit;   3 type;   0 defn)
%            Number of atoms       :   41 (   0 equality;  26 variable)
%            Maximal formula depth :   10 (   5 average)
%            Number of connectives :   33 (   0   ~;   0   |;   3   &;  19   @)
%                                         (   0 <=>;  11  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   17 (  17   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :   14 (   0 sgn;  10   !;   0   ?;   4   ^)
%                                         (  14   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(b_type,type,(
    b: $tType )).

thf(cQ,type,(
    cQ: ( b > $o ) > $o )).

thf(cP,type,(
    cP: ( b > $o ) > $o )).

thf(cTHM567_pme,conjecture,
    ( ( ! [S: ( b > $o ) > $o] :
          ( ! [Xx: b > $o] :
              ( ( S @ Xx )
             => ( cP @ Xx ) )
         => ( cP
            @ ^ [Xx: b] :
              ! [S0: b > $o] :
                ( ( S @ S0 )
               => ( S0 @ Xx ) ) ) )
      & ! [S: ( b > $o ) > $o] :
          ( ! [Xx: b > $o] :
              ( ( S @ Xx )
             => ( cQ @ Xx ) )
         => ( cQ
            @ ^ [Xx: b] :
              ! [S0: b > $o] :
                ( ( S @ S0 )
               => ( S0 @ Xx ) ) ) ) )
   => ! [S: ( b > $o ) > $o] :
        ( ! [Xx: b > $o] :
            ( ( S @ Xx )
           => ( ( cP @ Xx )
              & ( cQ @ Xx ) ) )
       => ( ( cP
            @ ^ [Xx: b] :
              ! [S0: b > $o] :
                ( ( S @ S0 )
               => ( S0 @ Xx ) ) )
          & ( cQ
            @ ^ [Xx: b] :
              ! [S0: b > $o] :
                ( ( S @ S0 )
               => ( S0 @ Xx ) ) ) ) ) )).

%------------------------------------------------------------------------------
