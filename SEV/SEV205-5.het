logic THF

spec SEV2055 =
%------------------------------------------------------------------------------
% File     : SEV205^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem from S-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1158 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    7 (   4 unit;   6 type;   0 defn)
%            Number of atoms       :   89 (  13 equality;  48 variable)
%            Maximal formula depth :   14 (   4 average)
%            Number of connectives :   54 (   2   ~;   0   |;  11   &;  35   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    7 (   7   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    8 (   6   :)
%            Number of variables   :   22 (   0 sgn;  21   !;   1   ?;   0   ^)
%                                         (  22   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(b_type,type,(
    b: $tType )).

thf(iS_type,type,(
    iS: $tType )).

thf(cP2,type,(
    cP2: b > b > b )).

thf(cP,type,(
    cP: iS > iS > iS )).

thf(c02,type,(
    c02: b )).

thf(c0,type,(
    c0: iS )).

thf(cTHM_S_INIT_pme,conjecture,
    ( ( ! [Xx: iS,Xy: iS] :
          ( ( cP @ Xx @ Xy )
         != c0 )
      & ! [Xx: iS,Xy: iS,Xu: iS,Xv: iS] :
          ( ( ( cP @ Xx @ Xu )
            = ( cP @ Xy @ Xv ) )
         => ( ( Xx = Xy )
            & ( Xu = Xv ) ) )
      & ! [X: iS > $o] :
          ( ( ( X @ c0 )
            & ! [Xx: iS,Xy: iS] :
                ( ( ( X @ Xx )
                  & ( X @ Xy ) )
               => ( X @ ( cP @ Xx @ Xy ) ) ) )
         => ! [Xx: iS] :
              ( X @ Xx ) )
      & ! [Xx: b,Xy: b,Xu: b,Xv: b] :
          ( ( ( cP2 @ Xx @ Xu )
            = ( cP2 @ Xy @ Xv ) )
         => ( ( Xx = Xy )
            & ( Xu = Xv ) ) )
      & ! [Xx: b,Xy: b] :
          ( ( cP2 @ Xx @ Xy )
         != c02 ) )
   => ? [Xf: iS > b] :
        ( ( ( Xf @ c0 )
          = c02 )
        & ! [Xx: iS,Xy: iS] :
            ( ( Xf @ ( cP @ Xx @ Xy ) )
            = ( cP2 @ ( Xf @ Xx ) @ ( Xf @ Xy ) ) )
        & ! [Xg: iS > b] :
            ( ( ( ( Xg @ c0 )
                = c02 )
              & ! [Xx: iS,Xy: iS] :
                  ( ( Xg @ ( cP @ Xx @ Xy ) )
                  = ( cP2 @ ( Xg @ Xx ) @ ( Xg @ Xy ) ) ) )
           => ( Xf = Xg ) ) ) )).

%------------------------------------------------------------------------------
