logic THF

spec SEV3575 =
%------------------------------------------------------------------------------
% File     : SEV357^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (GvNB)
% Problem  : TPS problem from GVB-MB-AXIOMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0834 [Bro09]

% Status   : CounterSatisfiable
% Rating   : 0.33 v5.4.0, 1.00 v5.0.0, 0.33 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    6 (   1 unit;   5 type;   0 defn)
%            Number of atoms       :   28 (   1 equality;   9 variable)
%            Maximal formula depth :   12 (   5 average)
%            Number of connectives :   15 (   1   ~;   0   |;   4   &;   9   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    7 (   5   :)
%            Number of variables   :    3 (   0 sgn;   1   !;   2   ?;   0   ^)
%                                         (   3   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cGVB_OP,type,(
    cGVB_OP: $i > $i > $i )).

thf(cGVB_IN,type,(
    cGVB_IN: $i > $i > $o )).

thf(cGVB_M,type,(
    cGVB_M: $i > $o )).

thf(cGVB_ZERO,type,(
    cGVB_ZERO: $i )).

thf(cGVB_FUNCTION,type,(
    cGVB_FUNCTION: $i > $o )).

thf(cGVB_E,conjecture,(
    ? [Xu: $i] :
      ( ( cGVB_FUNCTION @ Xu )
      & ! [Xx: $i] :
          ( ( ( cGVB_M @ Xx )
            & ( Xx != cGVB_ZERO ) )
         => ? [Xy: $i] :
              ( ( cGVB_M @ Xy )
              & ( cGVB_IN @ Xy @ Xx )
              & ( cGVB_IN @ ( cGVB_OP @ Xx @ Xy ) @ Xu ) ) ) ) )).

%------------------------------------------------------------------------------
