logic THF

spec SEV0217 =
%------------------------------------------------------------------------------
% File     : SEV021^7 : TPTP v6.2.0. Released v5.5.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from EQUIVALENCE-RELATIONS-THMS
% Version  : Especial.
% English  :

% Refs     : [Sul12] Sultana (2012), Email to Geoff Sutcliffe
% Source   : [Sul12]
% Names    : 

% Status   : Theorem
% Rating   : 1.00 v5.5.0
% Syntax   : Number of formulae    :    5 (   1 unit;   3 type;   1 defn)
%            Number of atoms       :   53 (   3 equality;  33 variable)
%            Maximal formula depth :   15 (   7 average)
%            Number of connectives :   38 (   0   ~;   0   |;   9   &;  20   @)
%                                         (   2 <=>;   7  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   12 (  12   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :   17 (   0 sgn;  11   !;   3   ?;   3   ^)
%                                         (  17   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : .
%          : The conjecture is of the form A => B, where A is not needed to
%            prove B. A is an easily provable property of equality.
%          : This version has the relation Q instantiated.
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cP,type,(
    cP: ( a > $o ) > $o )).

thf(cQ,type,(
    cQ: a > a > $o )).

thf(cQ_def,definition,
    ( cQ
    = ( ^ [X: a,Y: a] :
        ! [S: a > $o] :
          ( ( cP @ S )
         => ( ( S @ X )
          <=> ( S @ Y ) ) ) ) )).

thf(cTHM262_D_EXT2_pme,conjecture,
    ( ! [Xq1: a > $o,Xq2: a > $o] :
        ( ( ( Xq1 = Xq2 )
          & ( cP @ Xq1 ) )
       => ( cP @ Xq2 ) )
   => ( ( ! [Xp: a > $o] :
            ( ( cP @ Xp )
           => ? [Xz: a] :
                ( Xp @ Xz ) )
        & ! [Xx: a] :
          ? [Xp: a > $o] :
            ( ( cP @ Xp )
            & ( Xp @ Xx ) )
        & ! [Xx: a,Xy: a,Xp: a > $o,Xq: a > $o] :
            ( ( ( cP @ Xp )
              & ( cP @ Xq )
              & ( Xp @ Xx )
              & ( Xq @ Xx )
              & ( Xp @ Xy ) )
           => ( Xq @ Xy ) ) )
     => ( ( ^ [Xs: a > $o] :
              ( ? [Xz: a] :
                  ( Xs @ Xz )
              & ! [Xx: a] :
                  ( ( Xs @ Xx )
                 => ! [Xy: a] :
                      ( ( Xs @ Xy )
                    <=> ( cQ @ Xx @ Xy ) ) ) ) )
        = cP ) ) )).

%------------------------------------------------------------------------------
