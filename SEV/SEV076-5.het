logic THF

spec SEV0765 =
%------------------------------------------------------------------------------
% File     : SEV076^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem THM401B
% Version  : Especial.
% English  : In a complete lattice, every set has an upper bound.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0556 [Bro09]
%          : tps_0557 [Bro09]
%          : THM401A [TPS]
%          : THM401B [TPS]
%          : THM401 [TPS]
%          : THM401Z [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.0.0, 0.00 v4.0.1, 0.33 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   30 (   0 equality;  29 variable)
%            Maximal formula depth :   13 (   8 average)
%            Number of connectives :   28 (   0   ~;   0   |;   3   &;  19   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   12 (   0 sgn;  11   !;   1   ?;   0   ^)
%                                         (  12   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM401B_pme,conjecture,(
    ! [RRR: a > a > $o,U: ( a > $o ) > a] :
      ( ( ! [Xx: a,Xy: a,Xz: a] :
            ( ( ( RRR @ Xx @ Xy )
              & ( RRR @ Xy @ Xz ) )
           => ( RRR @ Xx @ Xz ) )
        & ! [Xs: a > $o] :
            ( ! [Xz: a] :
                ( ( Xs @ Xz )
               => ( RRR @ Xz @ ( U @ Xs ) ) )
            & ! [Xj: a] :
                ( ! [Xk: a] :
                    ( ( Xs @ Xk )
                   => ( RRR @ Xk @ Xj ) )
               => ( RRR @ ( U @ Xs ) @ Xj ) ) ) )
     => ! [Xs: a > $o] :
        ? [Xb: a] :
        ! [Xz: a] :
          ( ( Xs @ Xz )
         => ( RRR @ Xz @ Xb ) ) ) )).

%------------------------------------------------------------------------------
