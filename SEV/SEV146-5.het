logic THF

spec SEV1465 =
%------------------------------------------------------------------------------
% File     : SEV146^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from TRANSITIVE-CLOSURE
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1133 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.0.0, 0.00 v5.1.0, 0.25 v5.0.0, 0.00 v4.1.0, 0.33 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   60 (   0 equality;  59 variable)
%            Maximal formula depth :   17 (  10 average)
%            Number of connectives :   58 (   0   ~;   0   |;  10   &;  34   @)
%                                         (   0 <=>;  14  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   22 (   0 sgn;  22   !;   0   ?;   0   ^)
%                                         (  22   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM525_pme,conjecture,(
    ! [Xr: a > a > $o] :
      ( ! [Xx: a,Xy: a] :
          ( ( Xr @ Xx @ Xy )
         => ! [Xq: a > $o] :
              ( ( ! [Xw: a] :
                    ( ( Xr @ Xx @ Xw )
                   => ( Xq @ Xw ) )
                & ! [Xv: a,Xw: a] :
                    ( ( ( Xq @ Xv )
                      & ( Xr @ Xv @ Xw ) )
                   => ( Xq @ Xw ) ) )
             => ( Xq @ Xy ) ) )
      & ! [Xx: a,Xy: a,Xz: a] :
          ( ( ! [Xq: a > $o] :
                ( ( ! [Xw: a] :
                      ( ( Xr @ Xx @ Xw )
                     => ( Xq @ Xw ) )
                  & ! [Xv: a,Xw: a] :
                      ( ( ( Xq @ Xv )
                        & ( Xr @ Xv @ Xw ) )
                     => ( Xq @ Xw ) ) )
               => ( Xq @ Xy ) )
            & ! [Xq: a > $o] :
                ( ( ! [Xw: a] :
                      ( ( Xr @ Xy @ Xw )
                     => ( Xq @ Xw ) )
                  & ! [Xv: a,Xw: a] :
                      ( ( ( Xq @ Xv )
                        & ( Xr @ Xv @ Xw ) )
                     => ( Xq @ Xw ) ) )
               => ( Xq @ Xz ) ) )
         => ! [Xq: a > $o] :
              ( ( ! [Xw: a] :
                    ( ( Xr @ Xx @ Xw )
                   => ( Xq @ Xw ) )
                & ! [Xv: a,Xw: a] :
                    ( ( ( Xq @ Xv )
                      & ( Xr @ Xv @ Xw ) )
                   => ( Xq @ Xw ) ) )
             => ( Xq @ Xz ) ) ) ) )).

%------------------------------------------------------------------------------
