logic THF

spec SEV0105 =
%------------------------------------------------------------------------------
% File     : SEV010^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem THM260
% Version  : Especial.
% English  : An equivalence relation defines a partition.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0512 [Bro09]
%          : THM260 [TPS]

% Status   : Theorem
% Rating   : 0.86 v6.1.0, 0.71 v5.5.0, 0.83 v5.4.0, 0.60 v4.1.0, 0.67 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   55 (   1 equality;  53 variable)
%            Maximal formula depth :   19 (  10 average)
%            Number of connectives :   51 (   0   ~;   0   |;  10   &;  30   @)
%                                         (   3 <=>;   8  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   21 (   0 sgn;  16   !;   5   ?;   0   ^)
%                                         (  21   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM260_pme,conjecture,(
    ! [R: a > a > $o] :
      ( ( ! [Xx: a] :
            ( R @ Xx @ Xx )
        & ! [Xx: a,Xy: a] :
            ( ( R @ Xx @ Xy )
           => ( R @ Xy @ Xx ) )
        & ! [Xx: a,Xy: a,Xz: a] :
            ( ( ( R @ Xx @ Xy )
              & ( R @ Xy @ Xz ) )
           => ( R @ Xx @ Xz ) ) )
     => ( ! [Xp: a > $o] :
            ( ( ? [Xz: a] :
                  ( Xp @ Xz )
              & ! [Xx: a] :
                  ( ( Xp @ Xx )
                 => ! [Xy: a] :
                      ( ( Xp @ Xy )
                    <=> ( R @ Xx @ Xy ) ) ) )
           => ? [Xz: a] :
                ( Xp @ Xz ) )
        & ! [Xx: a] :
          ? [Xp: a > $o] :
            ( ? [Xz: a] :
                ( Xp @ Xz )
            & ! [Xx0: a] :
                ( ( Xp @ Xx0 )
               => ! [Xy: a] :
                    ( ( Xp @ Xy )
                  <=> ( R @ Xx0 @ Xy ) ) )
            & ( Xp @ Xx )
            & ! [Xq: a > $o] :
                ( ( ? [Xz: a] :
                      ( Xq @ Xz )
                  & ! [Xx0: a] :
                      ( ( Xq @ Xx0 )
                     => ! [Xy: a] :
                          ( ( Xq @ Xy )
                        <=> ( R @ Xx0 @ Xy ) ) )
                  & ( Xq @ Xx ) )
               => ( Xq = Xp ) ) ) ) ) )).

%------------------------------------------------------------------------------
