logic THF

spec SEV0605 =
%------------------------------------------------------------------------------
% File     : SEV060^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem THM173
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0329 [Bro09]
%          : THM173 [TPS]

% Status   : Theorem
% Rating   : 0.29 v5.5.0, 0.17 v5.4.0, 0.20 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   2 unit;   2 type;   0 defn)
%            Number of atoms       :   23 (   2 equality;  19 variable)
%            Maximal formula depth :   13 (   6 average)
%            Number of connectives :   17 (   1   ~;   1   |;   2   &;  10   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   2   :)
%            Number of variables   :    8 (   0 sgn;   8   !;   0   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(b_type,type,(
    b: $tType )).

thf(a_type,type,(
    a: $tType )).

thf(cTHM173_pme,conjecture,(
    ! [Xx: b,Xy: a,Xs: b > a > $o,Xk: b > a > $o] :
      ( ( ! [Xx_0: b,Xy_9: a] :
            ( ( Xk @ Xx_0 @ Xy_9 )
           => ( ( Xs @ Xx_0 @ Xy_9 )
              | ( ( Xx_0 = Xx )
                & ( Xy_9 = Xy ) ) ) )
        & ~ ( Xk @ Xx @ Xy ) )
     => ! [Xx0: b,Xy0: a] :
          ( ( Xk @ Xx0 @ Xy0 )
         => ( Xs @ Xx0 @ Xy0 ) ) ) )).

%------------------------------------------------------------------------------
