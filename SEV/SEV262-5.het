logic THF

spec SEV2625 =
%------------------------------------------------------------------------------
% File     : SEV262^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem NBHD-THM2
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0551 [Bro09]
%          : NBHD-THM2 [TPS]

% Status   : Theorem
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   29 (   1 equality;  27 variable)
%            Maximal formula depth :   15 (   8 average)
%            Number of connectives :   25 (   0   ~;   0   |;   5   &;  13   @)
%                                         (   1 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   12 (   0 sgn;   8   !;   3   ?;   1   ^)
%                                         (  12   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cNBHD_THM2_pme,conjecture,(
    ! [T: ( a > $o ) > $o] :
      ( ! [K: ( a > $o ) > $o,R: a > $o] :
          ( ( ! [Xx: a > $o] :
                ( ( K @ Xx )
               => ( T @ Xx ) )
            & ( R
              = ( ^ [Xx: a] :
                  ? [S: a > $o] :
                    ( ( K @ S )
                    & ( S @ Xx ) ) ) ) )
         => ( T @ R ) )
     => ! [S: a > $o] :
          ( ( T @ S )
        <=> ! [Xx: a] :
              ( ( S @ Xx )
             => ? [R: a > $o] :
                  ( ? [N: a > $o] :
                      ( ( T @ N )
                      & ! [Xx0: a] :
                          ( ( N @ Xx0 )
                         => ( R @ Xx0 ) )
                      & ( N @ Xx ) )
                  & ! [Xx0: a] :
                      ( ( R @ Xx0 )
                     => ( S @ Xx0 ) ) ) ) ) ) )).

%------------------------------------------------------------------------------
