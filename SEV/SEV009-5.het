logic THF

spec SEV0095 =
%------------------------------------------------------------------------------
% File     : SEV009^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem THM261-B
% Version  : Especial.
% English  : A partition defines an equivalence relation.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0511 [Bro09]
%          : THM261-B [TPS]

% Status   : Theorem
% Rating   : 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.1.0, 0.40 v5.0.0, 0.20 v4.1.0, 0.00 v4.0.1, 0.67 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   48 (   1 equality;  46 variable)
%            Maximal formula depth :   14 (   8 average)
%            Number of connectives :   44 (   0   ~;   0   |;  18   &;  22   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   10 (  10   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   16 (   0 sgn;   9   !;   7   ?;   0   ^)
%                                         (  16   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM261_B_pme,conjecture,(
    ! [P: ( a > $o ) > $o] :
      ( ! [Xx: a] :
        ? [Xp: a > $o] :
          ( ( P @ Xp )
          & ( Xp @ Xx )
          & ! [Xq: a > $o] :
              ( ( ( P @ Xq )
                & ( Xq @ Xx ) )
             => ( Xq = Xp ) ) )
     => ( ! [Xx: a] :
          ? [S: a > $o] :
            ( ( P @ S )
            & ( S @ Xx )
            & ( S @ Xx ) )
        & ! [Xx: a,Xy: a] :
            ( ? [S: a > $o] :
                ( ( P @ S )
                & ( S @ Xx )
                & ( S @ Xy ) )
           => ? [S: a > $o] :
                ( ( P @ S )
                & ( S @ Xy )
                & ( S @ Xx ) ) )
        & ! [Xx: a,Xy: a,Xz: a] :
            ( ( ? [S: a > $o] :
                  ( ( P @ S )
                  & ( S @ Xx )
                  & ( S @ Xy ) )
              & ? [S: a > $o] :
                  ( ( P @ S )
                  & ( S @ Xy )
                  & ( S @ Xz ) ) )
           => ? [S: a > $o] :
                ( ( P @ S )
                & ( S @ Xx )
                & ( S @ Xz ) ) ) ) ) )).

%------------------------------------------------------------------------------
