logic THF

spec SEV3855 =
%------------------------------------------------------------------------------
% File     : SEV385^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory
% Problem  : TPS problem X6004
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0248 [Bro09]
%          : X6004 [TPS]

% Status   : Theorem
% Rating   : 0.29 v6.1.0, 0.14 v5.5.0, 0.33 v5.4.0, 0.40 v5.3.0, 0.20 v5.2.0, 0.40 v4.1.0, 0.33 v4.0.1, 0.67 v4.0.0
% Syntax   : Number of formulae    :    5 (   4 unit;   4 type;   0 defn)
%            Number of atoms       :   26 (   7 equality;  11 variable)
%            Maximal formula depth :   11 (   4 average)
%            Number of connectives :    7 (   0   ~;   0   |;   2   &;   3   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   4   :)
%            Number of variables   :    7 (   0 sgn;   2   !;   2   ?;   3   ^)
%                                         (   7   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : Polymorphic definitions expanded.
%          : 
%------------------------------------------------------------------------------
thf(b_type,type,(
    b: $tType )).

thf(a_type,type,(
    a: $tType )).

thf(x,type,(
    x: b )).

thf(y,type,(
    y: a )).

thf(cX6004_pme,conjecture,(
    ? [Xs: b > a] :
      ( ! [Xx_6: b] :
          ( ( x = Xx_6 )
         => ( y
            = ( Xs @ Xx_6 ) ) )
      & ! [Xy_56: a] :
          ( ( y = Xy_56 )
         => ? [Xy0: b] :
              ( ( ^ [Xx_7: b] :
                    ( ( x = Xx_7 )
                    & ( Xy_56
                      = ( Xs @ Xx_7 ) ) ) )
              = ( ^ [Xx: b,Xy: b] : ( Xx = Xy )
                @ Xy0 ) ) ) ) )).

%------------------------------------------------------------------------------
