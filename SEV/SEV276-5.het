logic THF

spec SEV2765 =
%------------------------------------------------------------------------------
% File     : SEV276^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Sets of sets)
% Problem  : TPS problem from WELL-ORD-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0954 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   1 unit;   2 type;   0 defn)
%            Number of atoms       :   32 (   2 equality;  22 variable)
%            Maximal formula depth :   14 (   7 average)
%            Number of connectives :   23 (   0   ~;   0   |;   4   &;  13   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :)
%            Number of variables   :    8 (   0 sgn;   6   !;   2   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cR,type,(
    cR: a > a > $o )).

thf(cTHM544_pme,conjecture,
    ( ! [X: a > $o] :
        ( ? [Xz: a] :
            ( X @ Xz )
       => ? [Xz: a] :
            ( ( X @ Xz )
            & ! [Xx: a] :
                ( ( X @ Xx )
               => ( cR @ Xz @ Xx ) )
            & ! [Xy: a] :
                ( ( ( X @ Xy )
                  & ! [Xx: a] :
                      ( ( X @ Xx )
                     => ( cR @ Xy @ Xx ) ) )
               => ( Xy = Xz ) ) ) )
   => ! [Xx: a,Xy: a] :
        ( ( ( cR @ Xx @ Xy )
          & ( cR @ Xy @ Xx ) )
       => ( Xx = Xy ) ) )).

%------------------------------------------------------------------------------
