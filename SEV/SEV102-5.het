logic THF

spec SEV1025 =
%------------------------------------------------------------------------------
% File     : SEV102^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Set Theory (Relations)
% Problem  : TPS problem from RELN-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1099 [Bro09]

% Status   : Unknown
% Rating   : 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   55 (   9 equality;  45 variable)
%            Maximal formula depth :   16 (   9 average)
%            Number of connectives :   35 (   0   ~;   0   |;   7   &;  21   @)
%                                         (   0 <=>;   7  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   24 (   0 sgn;   9   !;   6   ?;   9   ^)
%                                         (  24   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_UNK

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cEQP_1C_pme,conjecture,(
    ! [Xx: a > $o,Xy: a > $o,Xz: a > $o] :
      ( ( ? [Xs: a > a] :
            ( ! [Xx0: a] :
                ( ( Xx @ Xx0 )
               => ( Xy @ ( Xs @ Xx0 ) ) )
            & ! [Xy0: a] :
                ( ( Xy @ Xy0 )
               => ? [Xy_52: a] :
                    ( ( ^ [Xx0: a] :
                          ( ( Xx @ Xx0 )
                          & ( Xy0
                            = ( Xs @ Xx0 ) ) ) )
                    = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                      @ Xy_52 ) ) ) )
        & ? [Xs: a > a] :
            ( ! [Xx0: a] :
                ( ( Xy @ Xx0 )
               => ( Xz @ ( Xs @ Xx0 ) ) )
            & ! [Xy0: a] :
                ( ( Xz @ Xy0 )
               => ? [Xy_53: a] :
                    ( ( ^ [Xx0: a] :
                          ( ( Xy @ Xx0 )
                          & ( Xy0
                            = ( Xs @ Xx0 ) ) ) )
                    = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                      @ Xy_53 ) ) ) ) )
     => ? [Xs: a > a] :
          ( ! [Xx0: a] :
              ( ( Xx @ Xx0 )
             => ( Xz @ ( Xs @ Xx0 ) ) )
          & ! [Xy0: a] :
              ( ( Xz @ Xy0 )
             => ? [Xy_55: a] :
                  ( ( ^ [Xx0: a] :
                        ( ( Xx @ Xx0 )
                        & ( Xy0
                          = ( Xs @ Xx0 ) ) ) )
                  = ( ^ [Xx: a,Xy: a] : ( Xx = Xy )
                    @ Xy_55 ) ) ) ) ) )).

%------------------------------------------------------------------------------
