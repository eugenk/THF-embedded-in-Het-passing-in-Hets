logic THF

spec SYN9951 =
%------------------------------------------------------------------------------
% File     : SYN995^1 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Syntactic
% Problem  : Every function has a fixed point
% Version  : Especial.
% English  :

% Refs     : [BB05]  Benzmueller & Brown (2005), A Structured Set of Higher
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : Example 3 [BB05]

% Status   : CounterSatisfiable
% Rating   : 0.33 v4.1.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    3 (   0 unit;   1 type;   1 defn)
%            Number of atoms       :   13 (   1 equality;   7 variable)
%            Maximal formula depth :    7 (   6 average)
%            Number of connectives :    6 (   0   ~;   0   |;   0   &;   5   @)
%                                         (   1 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :    5 (   0 sgn;   2   !;   1   ?;   2   ^)
%                                         (   5   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU

% Comments : 
%------------------------------------------------------------------------------
thf(leibeq_decl,type,(
    leibeq: $i > $i > $o )).

thf(leibeq,definition,
    ( leibeq
    = ( ^ [X: $i,Y: $i] :
        ! [P: $i > $o] :
          ( ( P @ X )
        <=> ( P @ Y ) ) ) )).

thf(conj,conjecture,(
    ! [F: $i > $i] :
    ? [X: $i] :
      ( leibeq @ ( F @ X ) @ X ) )).

%------------------------------------------------------------------------------
