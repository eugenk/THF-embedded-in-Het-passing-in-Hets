logic THF

spec NUM6411 =
%------------------------------------------------------------------------------
% File     : NUM641^1 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Number Theory
% Problem  : Landau theorem 4g
% Version  : Especial.
% English  : suc x = pl n_1 x

% Refs     : [Lan30] Landau (1930), Grundlagen der Analysis
%          : [vBJ79] van Benthem Jutting (1979), Checking Landau's "Grundla
%          : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : satz4g [Lan30]

% Status   : Theorem
%          : Without extensionality : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    7 (   3 unit;   5 type;   0 defn)
%            Number of atoms       :   20 (   2 equality;   2 variable)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    6 (   0   ~;   0   |;   0   &;   6   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    6 (   5   :)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?;   0   ^)
%                                         (   1   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : 
%------------------------------------------------------------------------------
thf(nat_type,type,(
    nat: $tType )).

thf(x,type,(
    x: nat )).

thf(suc,type,(
    suc: nat > nat )).

thf(pl,type,(
    pl: nat > nat > nat )).

thf(n_1,type,(
    n_1: nat )).

thf(satz4c,axiom,(
    ! [Xx: nat] :
      ( ( pl @ n_1 @ Xx )
      = ( suc @ Xx ) ) )).

thf(satz4g,conjecture,
    ( ( suc @ x )
    = ( pl @ n_1 @ x ) )).

%------------------------------------------------------------------------------
