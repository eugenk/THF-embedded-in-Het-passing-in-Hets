logic THF

spec NUM8135 =
%------------------------------------------------------------------------------
% File     : NUM813^5 : TPTP v6.2.0. Bugfixed v5.2.0.
% Domain   : Number Theory (Induction on naturals)
% Problem  : TPS problem THM303
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0571 [Bro09]
%          : THM303 [TPS]

% Status   : Theorem
% Rating   : 1.00 v5.2.0
% Syntax   : Number of formulae    :    8 (   2 unit;   6 type;   1 defn)
%            Number of atoms       :   47 (   1 equality;  15 variable)
%            Maximal formula depth :   11 (   4 average)
%            Number of connectives :   33 (   0   ~;   1   |;   6   &;  20   @)
%                                         (   1 <=>;   5  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    8 (   6   :)
%            Number of variables   :    7 (   0 sgn;   7   !;   0   ?;   0   ^)
%                                         (   7   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
% Bugfixes : v5.2.0 - Added missing type declarations.
%------------------------------------------------------------------------------
thf(c0_type,type,(
    c0: $i )).

thf(cEVEN_type,type,(
    cEVEN: $i > $o )).

thf(cNUMBER_type,type,(
    cNUMBER: $i > $o )).

thf(cODD_type,type,(
    cODD: $i > $o )).

thf(cS_type,type,(
    cS: $i > $i )).

thf(cIND_type,type,(
    cIND: $o )).

thf(cIND_def,definition,
    ( cIND
    = ( ! [Xp: $i > $o] :
          ( ( ( Xp @ c0 )
            & ! [Xx: $i] :
                ( ( Xp @ Xx )
               => ( Xp @ ( cS @ Xx ) ) ) )
         => ! [Xx: $i] :
              ( Xp @ Xx ) ) ) )).

thf(cTHM303,conjecture,
    ( ( ( cEVEN @ c0 )
      & ! [Xn: $i] :
          ( ( cEVEN @ Xn )
         => ( cEVEN @ ( cS @ ( cS @ Xn ) ) ) )
      & ( cODD @ ( cS @ c0 ) )
      & ! [Xn: $i] :
          ( ( cODD @ Xn )
         => ( cODD @ ( cS @ ( cS @ Xn ) ) ) )
      & cIND
      & ! [Xn: $i] :
          ( ( cNUMBER @ Xn )
        <=> ( ( cEVEN @ Xn )
            | ( cODD @ Xn ) ) ) )
   => ! [Xn: $i] :
        ( cNUMBER @ Xn ) )).

%------------------------------------------------------------------------------
