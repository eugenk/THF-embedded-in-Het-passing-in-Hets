logic THF

spec NUM7081 =
%------------------------------------------------------------------------------
% File     : NUM708^1 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Number Theory
% Problem  : Landau theorem 28g
% Version  : Especial.
% English  : x = ts n_1 x

% Refs     : [Lan30] Landau (1930), Grundlagen der Analysis
%          : [vBJ79] van Benthem Jutting (1979), Checking Landau's "Grundla
%          : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : satz28g [Lan30]

% Status   : Theorem
%          : Without extensionality : Theorem
% Rating   : 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v5.2.0, 0.20 v4.1.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    6 (   3 unit;   4 type;   0 defn)
%            Number of atoms       :   16 (   2 equality;   2 variable)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    4 (   0   ~;   0   |;   0   &;   4   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    2 (   2   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   4   :)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?;   0   ^)
%                                         (   1   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : 
%------------------------------------------------------------------------------
thf(nat_type,type,(
    nat: $tType )).

thf(x,type,(
    x: nat )).

thf(ts,type,(
    ts: nat > nat > nat )).

thf(n_1,type,(
    n_1: nat )).

thf(satz28c,axiom,(
    ! [Xx: nat] :
      ( ( ts @ n_1 @ Xx )
      = Xx ) )).

thf(satz28g,conjecture,
    ( x
    = ( ts @ n_1 @ x ) )).

%------------------------------------------------------------------------------
