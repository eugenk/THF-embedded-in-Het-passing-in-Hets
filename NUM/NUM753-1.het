logic THF

spec NUM7531 =
%------------------------------------------------------------------------------
% File     : NUM753^1 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Number Theory
% Problem  : Landau theorem 62g
% Version  : Especial.
% English  : moref (pf x z) (pf y u)

% Refs     : [Lan30] Landau (1930), Grundlagen der Analysis
%          : [vBJ79] van Benthem Jutting (1979), Checking Landau's "Grundla
%          : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : satz62g [Lan30]

% Status   : Theorem
%          : Without extensionality : Theorem
% Rating   : 0.17 v6.2.0, 0.00 v6.1.0, 0.17 v6.0.0, 0.00 v5.5.0, 0.20 v5.4.0, 0.25 v5.3.0, 0.50 v5.2.0, 0.25 v5.1.0, 0.50 v5.0.0, 0.25 v4.1.0, 0.00 v4.0.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :   15 (   5 unit;   8 type;   0 defn)
%            Number of atoms       :   65 (   0 equality;  24 variable)
%            Maximal formula depth :   11 (   4 average)
%            Number of connectives :   44 (   0   ~;   0   |;   0   &;  38   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :   10 (   8   :)
%            Number of variables   :   12 (   0 sgn;  12   !;   0   ?;   0   ^)
%                                         (  12   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : 
%------------------------------------------------------------------------------
thf(frac_type,type,(
    frac: $tType )).

thf(x,type,(
    x: frac )).

thf(y,type,(
    y: frac )).

thf(z,type,(
    z: frac )).

thf(u,type,(
    u: frac )).

thf(eq,type,(
    eq: frac > frac > $o )).

thf(e,axiom,
    ( eq @ x @ y )).

thf(moref,type,(
    moref: frac > frac > $o )).

thf(m,axiom,
    ( moref @ z @ u )).

thf(pf,type,(
    pf: frac > frac > frac )).

thf(satz44,axiom,(
    ! [Xx: frac,Xy: frac,Xz: frac,Xu: frac] :
      ( ( moref @ Xx @ Xy )
     => ( ( eq @ Xx @ Xz )
       => ( ( eq @ Xy @ Xu )
         => ( moref @ Xz @ Xu ) ) ) ) )).

thf(satz62d,axiom,(
    ! [Xx: frac,Xy: frac,Xz: frac] :
      ( ( moref @ Xx @ Xy )
     => ( moref @ ( pf @ Xz @ Xx ) @ ( pf @ Xz @ Xy ) ) ) )).

thf(satz37,axiom,(
    ! [Xx: frac] :
      ( eq @ Xx @ Xx ) )).

thf(satz56,axiom,(
    ! [Xx: frac,Xy: frac,Xz: frac,Xu: frac] :
      ( ( eq @ Xx @ Xy )
     => ( ( eq @ Xz @ Xu )
       => ( eq @ ( pf @ Xx @ Xz ) @ ( pf @ Xy @ Xu ) ) ) ) )).

thf(satz62g,conjecture,
    ( moref @ ( pf @ x @ z ) @ ( pf @ y @ u ) )).

%------------------------------------------------------------------------------
