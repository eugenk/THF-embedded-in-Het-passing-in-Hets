logic THF

spec SYO5031 =
%------------------------------------------------------------------------------
% File     : SYO503^1 : TPTP v6.2.0. Released v4.1.0.
% Domain   : Syntactic
% Problem  : Tableau with two branches
% Version  : Especial.
% English  :

% Refs     : [BS09a] Brown & Smolka (2009), Terminating Tableaux for the Ba
%          : [BS09b] Brown E. & Smolka (2009), Extended First-Order Logic
%          : [Bro09] Brown E. (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : Example 3.4 [BS09a]
%          : basic9 [Bro09]

% Status   : Theorem
% Rating   : 0.43 v6.1.0, 0.29 v5.5.0, 0.17 v5.4.0, 0.40 v5.3.0, 0.60 v5.1.0, 0.80 v5.0.0, 0.60 v4.1.0
% Syntax   : Number of formulae    :    7 (   3 unit;   6 type;   0 defn)
%            Number of atoms       :   25 (   1 equality;   0 variable)
%            Maximal formula depth :    9 (   4 average)
%            Number of connectives :   17 (   5   ~;   6   |;   0   &;   6   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    8 (   6   :)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?;   0   ^)
%                                         (   0   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : The fragment of simple type theory that restricts equations to
%            base types and disallows lambda abstraction and quantification is
%            decidable. This is an example.
%------------------------------------------------------------------------------
thf(a,type,(
    a: $o )).

thf(b,type,(
    b: $o )).

thf(c,type,(
    c: $o )).

thf(f,type,(
    f: $o > $o )).

thf(g,type,(
    g: $o > $o )).

thf(p,type,(
    p: ( $o > $o ) > $o )).

thf(claim,conjecture,
    ( ( a = b )
    | ~ ( f @ a )
    | ~ ( f @ b )
    | ~ ( g @ a )
    | ~ ( g @ b )
    | ~ ( p @ f )
    | ( p @ g ) )).

%------------------------------------------------------------------------------
