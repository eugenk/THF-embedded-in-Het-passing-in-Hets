logic THF

spec SYO0865 =
%------------------------------------------------------------------------------
% File     : SYO086^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem THM50-11
% Version  : Especial.
% English  : Simple formula for debugging.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0121 [Bro09]
%          : THM50-11 [TPS]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   3 unit;   3 type;   0 defn)
%            Number of atoms       :   13 (   0 equality;   0 variable)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :   14 (   5   ~;   2   |;   5   &;   0   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?;   0   ^)
%                                         (   0   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cR,type,(
    cR: $o )).

thf(cQ,type,(
    cQ: $o )).

thf(cP,type,(
    cP: $o )).

thf(cTHM50_11,conjecture,
    ( ( ~ ( cP )
      & ( ( cQ
          & ~ ( cR ) )
        | ( ~ ( cQ )
          & cR ) ) )
   => ( ( ( cP
          & cQ )
        | ( ~ ( cP )
          & ~ ( cQ ) ) )
     => cR ) )).

%------------------------------------------------------------------------------
