logic THF

spec SYO0041 =
%------------------------------------------------------------------------------
% File     : SYO004^1 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Syntactic
% Problem  : Relating Leibniz equality to primitive equality
% Version  : Especial.
% English  :

% Refs     : [And72] Andrews (1972), General Models and Extensionality
%          : [BB05]  Benzmueller & Brown (2005), A Structured Set of Higher
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : Example 8  [BB05]

% Status   : Theorem
% Rating   : 0.29 v6.1.0, 0.00 v6.0.0, 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.2.0, 0.40 v4.1.0, 0.33 v3.7.0
% Syntax   : Number of formulae    :    3 (   0 unit;   1 type;   1 defn)
%            Number of atoms       :   15 (   2 equality;   8 variable)
%            Maximal formula depth :    7 (   6 average)
%            Number of connectives :    6 (   0   ~;   0   |;   0   &;   4   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :    5 (   0 sgn;   3   !;   0   ?;   2   ^)
%                                         (   5   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : Andrews property q is required
%          : 
%------------------------------------------------------------------------------
thf(leibeq_decl,type,(
    leibeq: $i > $i > $o )).

thf(leibeq,definition,
    ( leibeq
    = ( ^ [X: $i,Y: $i] :
        ! [P: $i > $o] :
          ( ( P @ X )
         => ( P @ Y ) ) ) )).

thf(conj,conjecture,(
    ! [X: $i,Y: $i] :
      ( ( leibeq @ X @ Y )
     => ( X = Y ) ) )).

%------------------------------------------------------------------------------
