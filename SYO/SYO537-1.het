logic THF

spec SYO5371 =
%------------------------------------------------------------------------------
% File     : SYO537^1 : TPTP v6.2.0. Released v5.2.0.
% Domain   : Syntactic
% Problem  : Choice on binary relations between functions
% Version  : Especial.
% English  : 

% Refs     : [Bro11] Brown E. (2011), Email to Geoff Sutcliffe
% Source   : [Bro11]
% Names    : CHOICE14 [Bro11]

% Status   : Theorem
% Rating   : 0.43 v6.1.0, 0.29 v5.5.0, 0.50 v5.4.0, 0.60 v5.2.0
% Syntax   : Number of formulae    :    7 (   0 unit;   3 type;   2 defn)
%            Number of atoms       :   45 (   2 equality;  16 variable)
%            Maximal formula depth :    8 (   6 average)
%            Number of connectives :   18 (   0   ~;   0   |;   0   &;  16   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   36 (  36   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :   10 (   0 sgn;   2   !;   4   ?;   4   ^)
%                                         (  10   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : A choice operator ii is used to define a choice operator on ii*ii 
%            (Curried).
%------------------------------------------------------------------------------
thf(epsii,type,(
    epsii: ( ( $i > $i ) > $o ) > $i > $i )).

thf(choiceaxii,axiom,(
    ! [P: ( $i > $i ) > $o] :
      ( ? [X: $i > $i] :
          ( P @ X )
     => ( P @ ( epsii @ P ) ) ) )).

thf(epsa,type,(
    epsa: ( ( $i > $i ) > ( $i > $i ) > $o ) > $i > $i )).

thf(epsad,definition,
    ( epsa
    = ( ^ [R: ( $i > $i ) > ( $i > $i ) > $o] :
          ( epsii
          @ ^ [X: $i > $i] :
            ? [Y: $i > $i] :
              ( R @ X @ Y ) ) ) )).

thf(epsb,type,(
    epsb: ( ( $i > $i ) > ( $i > $i ) > $o ) > $i > $i )).

thf(epsbd,definition,
    ( epsb
    = ( ^ [R: ( $i > $i ) > ( $i > $i ) > $o] :
          ( epsii
          @ ^ [Y: $i > $i] :
              ( R @ ( epsa @ R ) @ Y ) ) ) )).

thf(conj,conjecture,(
    ! [R: ( $i > $i ) > ( $i > $i ) > $o] :
      ( ? [X: $i > $i,Y: $i > $i] :
          ( R @ X @ Y )
     => ( R @ ( epsa @ R ) @ ( epsb @ R ) ) ) )).

%------------------------------------------------------------------------------
