logic THF

spec SYO2415 =
%------------------------------------------------------------------------------
% File     : SYO241^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-EQ-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0431 [Bro09]
%          : tps_0900 [Bro09]
%          : THM143A [TPS]

% Status   : Theorem
% Rating   : 0.29 v6.0.0, 0.43 v5.5.0, 0.17 v5.4.0, 0.40 v5.3.0, 0.60 v5.2.0, 0.80 v5.0.0, 0.60 v4.1.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    1 (   0 unit;   0 type;   0 defn)
%            Number of atoms       :   22 (   4 equality;  18 variable)
%            Maximal formula depth :   14 (  14 average)
%            Number of connectives :   16 (   3   ~;   0   |;   2   &;   9   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   0   :)
%            Number of variables   :    6 (   0 sgn;   3   !;   2   ?;   1   ^)
%                                         (   6   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cTHM143_EXPAND,conjecture,(
    ! [Xh: ( $i > $o ) > $i] :
      ( ! [Xp: $i > $o,Xq: $i > $o] :
          ( ( ( Xh @ Xp )
            = ( Xh @ Xq ) )
         => ( Xp = Xq ) )
     => ~ ( ? [Xt: $i > $o] :
              ( ~ ( Xt @ ( Xh @ Xt ) )
              & ( ( Xh
                  @ ^ [Xz: $i] :
                    ? [Xt0: $i > $o] :
                      ( ~ ( Xt0 @ ( Xh @ Xt0 ) )
                      & ( Xz
                        = ( Xh @ Xt0 ) ) ) )
                = ( Xh @ Xt ) ) ) ) ) )).

%------------------------------------------------------------------------------
