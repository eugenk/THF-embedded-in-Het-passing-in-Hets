logic THF

spec SYO0795 =
%------------------------------------------------------------------------------
% File     : SYO079^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem THM50-A
% Version  : Especial.
% English  : Associativity of equivalence.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0044 [Bro09]
%          : THM50-A [TPS]
%          : THM52 [TPS]

% Status   : Theorem
% Rating   : 0.17 v6.2.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   3 unit;   3 type;   0 defn)
%            Number of atoms       :    9 (   0 equality;   0 variable)
%            Maximal formula depth :    4 (   2 average)
%            Number of connectives :    5 (   0   ~;   0   |;   0   &;   0   @)
%                                         (   4 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    0 (   0   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   3   :)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?;   0   ^)
%                                         (   0   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cR,type,(
    cR: $o )).

thf(cQ,type,(
    cQ: $o )).

thf(cP,type,(
    cP: $o )).

thf(cTHM50_A,conjecture,
    ( ( ( cP
      <=> cQ )
    <=> cR )
   => ( cP
    <=> ( cQ
      <=> cR ) ) )).

%------------------------------------------------------------------------------
