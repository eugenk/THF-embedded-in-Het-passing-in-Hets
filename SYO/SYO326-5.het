logic THF

spec SYO3265 =
%------------------------------------------------------------------------------
% File     : SYO326^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_1004 [Bro09]

% Status   : Theorem
% Rating   : 0.83 v6.2.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    5 (   0 unit;   4 type;   0 defn)
%            Number of atoms       :   43 (   0 equality;  26 variable)
%            Maximal formula depth :   13 (   5 average)
%            Number of connectives :   33 (   0   ~;   0   |;   5   &;  22   @)
%                                         (   0 <=>;   6  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    9 (   9   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    6 (   4   :)
%            Number of variables   :   10 (   0 sgn;   9   !;   1   ?;   0   ^)
%                                         (  10   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cC,type,(
    cC: $i > $o )).

thf(f,type,(
    f: $i > $i )).

thf(cB,type,(
    cB: $i > $i > $o )).

thf(cA,type,(
    cA: $i > $o )).

thf(cSV8,conjecture,(
    ? [Xu: $i > $i > $o] :
      ( ! [Xw: $i,Xz: $i] :
          ( ( ( cA @ Xw )
            & ( cB @ Xz @ Xw ) )
         => ( Xu @ ( f @ Xw ) @ Xz ) )
      & ! [Xz: $i] :
          ( ( cC @ Xz )
         => ( Xu @ Xz @ Xz ) )
      & ! [Xv: $i > $i > $o] :
          ( ( ! [Xw: $i,Xz: $i] :
                ( ( ( cA @ Xw )
                  & ( cB @ Xz @ Xw ) )
               => ( Xv @ ( f @ Xw ) @ Xz ) )
            & ! [Xz: $i] :
                ( ( cC @ Xz )
               => ( Xv @ Xz @ Xz ) ) )
         => ! [Xx: $i,Xy: $i] :
              ( ( Xu @ Xx @ Xy )
             => ( Xv @ Xx @ Xy ) ) ) ) )).

%------------------------------------------------------------------------------
