logic THF

spec SYO2575 =
%------------------------------------------------------------------------------
% File     : SYO257^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem THM84
% Version  : Especial.
% English  : 

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0113 [Bro09]
%          : THM84 [TPS]

% Status   : Theorem
% Rating   : 0.17 v6.2.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   19 (   0 equality;   7 variable)
%            Maximal formula depth :    9 (   5 average)
%            Number of connectives :   11 (   0   ~;   0   |;   1   &;   9   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :    4 (   0 sgn;   3   !;   1   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(h,type,(
    h: $i > $i )).

thf(g,type,(
    g: $i > $i )).

thf(cP,type,(
    cP: $i > $i > $o )).

thf(cTHM84,conjecture,(
    ? [Xx: $i] :
    ! [Xy: $i] :
      ( ! [Xf: $i > $i,Xz: $i] :
          ( ( cP @ Xz @ ( Xf @ Xx ) )
          & ( cP @ Xx @ Xy ) )
     => ( cP @ Xy @ ( g @ ( h @ Xy ) ) ) ) )).

%------------------------------------------------------------------------------
