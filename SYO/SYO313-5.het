logic THF

spec SYO3135 =
%------------------------------------------------------------------------------
% File     : SYO313^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-HO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0906 [Bro09]

% Status   : CounterSatisfiable
% Rating   : 0.33 v4.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   23 (   0 equality;  16 variable)
%            Maximal formula depth :   11 (   8 average)
%            Number of connectives :   19 (   0   ~;   0   |;   2   &;  12   @)
%                                         (   0 <=>;   5  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :    8 (   0 sgn;   5   !;   3   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cELESS,type,(
    cELESS: $i > $i > $o )).

thf(cELUB,conjecture,(
    ! [A: $i > $o] :
      ( ( ? [Xr: $i] :
            ( A @ Xr )
        & ? [Xu: $i] :
          ! [Xx: $i] :
            ( ( A @ Xx )
           => ( cELESS @ Xx @ Xu ) ) )
     => ? [Xl: $i] :
          ( ! [Xx: $i] :
              ( ( A @ Xx )
             => ( cELESS @ Xx @ Xl ) )
          & ! [Xy: $i] :
              ( ! [Xx: $i] :
                  ( ( A @ Xx )
                 => ( cELESS @ Xx @ Xy ) )
             => ( cELESS @ Xl @ Xy ) ) ) ) )).

%------------------------------------------------------------------------------
