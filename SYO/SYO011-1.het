logic THF

spec SYO0111 =
%------------------------------------------------------------------------------
% File     : SYO011^1 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Syntactic
% Problem  : Invalid formula in model classes not requiring f
% Version  : Especial.
% English  :

% Refs     : [BB05]  Benzmueller & Brown (2005), A Structured Set of Higher
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : Example 14 [BB05]

% Status   : Theorem
%          : Without functional extensionality : CounterSatisfiable
% Rating   : 0.29 v6.1.0, 0.43 v6.0.0, 0.29 v5.5.0, 0.33 v5.4.0, 0.40 v5.3.0, 0.60 v4.1.0, 0.67 v4.0.1, 1.00 v3.7.0
% Syntax   : Number of formulae    :    5 (   0 unit;   3 type;   1 defn)
%            Number of atoms       :   22 (   1 equality;   7 variable)
%            Maximal formula depth :    7 (   5 average)
%            Number of connectives :   10 (   0   ~;   0   |;   1   &;   7   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    6 (   6   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   3   :)
%            Number of variables   :    5 (   0 sgn;   2   !;   0   ?;   3   ^)
%                                         (   5   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : 
%------------------------------------------------------------------------------
thf(leibeq_type,type,(
    leibeq: $i > $i > $o )).

thf(leibeq,definition,
    ( leibeq
    = ( ^ [U: $i,V: $i] :
        ! [Q: $i > $o] :
          ( ( Q @ U )
         => ( Q @ V ) ) ) )).

thf(p_type,type,(
    p: ( $i > $i ) > $o )).

thf(f_type,type,(
    f: $i > $i )).

thf(conj,conjecture,
    ( ( ! [X: $i] :
          ( leibeq @ ( f @ X ) @ X )
      & ( p
        @ ^ [X: $i] : X ) )
   => ( p @ f ) )).

%------------------------------------------------------------------------------
