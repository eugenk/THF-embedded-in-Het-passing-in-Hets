logic THF

spec SYO0985 =
%------------------------------------------------------------------------------
% File     : SYO098^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem THM65
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0289 [Bro09]
%          : THM65 [TPS]

% Status   : Theorem
% Rating   : 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   18 (   0 equality;   8 variable)
%            Maximal formula depth :    9 (   6 average)
%            Number of connectives :   13 (   2   ~;   0   |;   1   &;   8   @)
%                                         (   0 <=>;   2  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    5 (   2   :)
%            Number of variables   :    4 (   0 sgn;   2   !;   2   ?;   0   ^)
%                                         (   4   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cQ,type,(
    cQ: $i > $i > $o )).

thf(cR,type,(
    cR: $i > $i > $o )).

thf(cTHM65,conjecture,
    ( ! [W: $i] :
        ~ ( cR @ W @ W )
   => ? [X: $i,Y: $i] :
        ( ~ ( cR @ X @ Y )
        & ( ( cQ @ Y @ X )
         => ! [Z: $i] :
              ( cQ @ Z @ Z ) ) ) )).

%------------------------------------------------------------------------------
