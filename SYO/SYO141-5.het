logic THF

spec SYO1415 =
%------------------------------------------------------------------------------
% File     : SYO141^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-FO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0756 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v6.2.0, 0.17 v6.1.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    6 (   2 unit;   5 type;   0 defn)
%            Number of atoms       :   18 (   0 equality;   2 variable)
%            Maximal formula depth :    5 (   3 average)
%            Number of connectives :    9 (   0   ~;   0   |;   0   &;   5   @)
%                                         (   0 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    7 (   5   :)
%            Number of variables   :    2 (   0 sgn;   2   !;   0   ?;   0   ^)
%                                         (   2   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a,type,(
    a: $i )).

thf(cQ,type,(
    cQ: $i > $o )).

thf(x,type,(
    x: $i )).

thf(cR,type,(
    cR: $i > $o )).

thf(cP,type,(
    cP: $i > $o )).

thf(cADDHYP2,conjecture,
    ( ( ( cP @ a )
     => ! [Xx0: $i] :
          ( cQ @ Xx0 ) )
   => ( ! [Xx0: $i] :
          ( cP @ Xx0 )
     => ( ( cR @ x )
       => ( cQ @ a ) ) ) )).

%------------------------------------------------------------------------------
