logic THF

spec SYO1615 =
%------------------------------------------------------------------------------
% File     : SYO161^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-FO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0868 [Bro09]

% Status   : Theorem
% Rating   : 0.17 v6.0.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   0 unit;   1 type;   0 defn)
%            Number of atoms       :   18 (   0 equality;   8 variable)
%            Maximal formula depth :    7 (   5 average)
%            Number of connectives :   15 (   0   ~;   0   |;   2   &;   8   @)
%                                         (   0 <=>;   5  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    1 (   1   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :    6 (   0 sgn;   3   !;   3   ?;   0   ^)
%                                         (   6   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cP,type,(
    cP: $i > $o )).

thf(cX2125_HALF,conjecture,
    ( ? [Xx: $i] :
      ! [Xy: $i] :
        ( ( ( cP @ Xx )
         => ( cP @ Xy ) )
        & ( ( cP @ Xy )
         => ( cP @ Xx ) ) )
   => ( ( ? [Xx: $i] :
            ( cP @ Xx )
       => ! [Xy: $i] :
            ( cP @ Xy ) )
      & ( ! [Xy: $i] :
            ( cP @ Xy )
       => ? [Xx: $i] :
            ( cP @ Xx ) ) ) )).

%------------------------------------------------------------------------------
