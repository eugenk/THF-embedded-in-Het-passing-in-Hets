logic THF

spec SYO0091 =
%------------------------------------------------------------------------------
% File     : SYO009^1 : TPTP v6.2.0. Released v3.7.0.
% Domain   : Syntactic
% Problem  : Eta-equality using Leibniz equality
% Version  : Especial.
% English  :

% Refs     : [BB05]  Benzmueller & Brown (2005), A Structured Set of Higher
%          : [Ben09] Benzmueller (2009), Email to Geoff Sutcliffe
% Source   : [Ben09]
% Names    : Example 12 [BB05]

% Status   : Theorem
%          : Without eta extensionality : CounterSatisfiable
% Rating   : 0.17 v6.2.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.00 v3.7.0
% Syntax   : Number of formulae    :    3 (   0 unit;   2 type;   0 defn)
%            Number of atoms       :   10 (   0 equality;   1 variable)
%            Maximal formula depth :    5 (   4 average)
%            Number of connectives :    4 (   0   ~;   0   |;   0   &;   3   @)
%                                         (   0 <=>;   1  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    4 (   2   :)
%            Number of variables   :    1 (   0 sgn;   0   !;   0   ?;   1   ^)
%                                         (   1   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : 
%------------------------------------------------------------------------------
thf(p,type,(
    p: ( $i > $i ) > $o )).

thf(f,type,(
    f: $i > $i )).

thf(conj,conjecture,
    ( ( p
      @ ^ [X: $i] :
          ( f @ X ) )
   => ( p @ f ) )).

%------------------------------------------------------------------------------
