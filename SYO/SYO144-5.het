logic THF

spec SYO1445 =
%------------------------------------------------------------------------------
% File     : SYO144^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem from BASIC-FO-THMS
% Version  : Especial.
% English  :

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0782 [Bro09]

% Status   : CounterSatisfiable
% Rating   : 0.00 v4.0.0
% Syntax   : Number of formulae    :    8 (   2 unit;   7 type;   0 defn)
%            Number of atoms       :   23 (   0 equality;   2 variable)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :   10 (   0   ~;   2   |;   0   &;   5   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    9 (   7   :)
%            Number of variables   :    2 (   0 sgn;   2   !;   0   ?;   0   ^)
%                                         (   2   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cC,type,(
    cC: $o )).

thf(x,type,(
    x: $i )).

thf(cR3,type,(
    cR3: $i > $o )).

thf(cR2,type,(
    cR2: $i > $o )).

thf(cR1,type,(
    cR1: $i > $o )).

thf(cQ,type,(
    cQ: $i > $o )).

thf(cP,type,(
    cP: $i > $o )).

thf(cADDHYP7,conjecture,
    ( ( ! [Xx0: $i] :
          ( cP @ Xx0 )
     => ! [Xx0: $i] :
          ( cQ @ Xx0 ) )
   => ( ( ( cR1 @ x )
        | ( cR2 @ x )
        | ( cR3 @ x ) )
     => cC ) )).

%------------------------------------------------------------------------------
