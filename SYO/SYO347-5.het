logic THF

spec SYO3475 =
%------------------------------------------------------------------------------
% File     : SYO347^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Syntactic
% Problem  : TPS problem THM620
% Version  : Especial.
% English  : Simple extensionality example.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0050 [Bro09]
%          : THM619 [TPS]
%          : THM620 [TPS]

% Status   : Theorem
% Rating   : 0.14 v5.5.0, 0.17 v5.4.0, 0.20 v5.3.0, 0.40 v4.1.0, 0.33 v4.0.0
% Syntax   : Number of formulae    :    4 (   0 unit;   3 type;   0 defn)
%            Number of atoms       :   14 (   1 equality;   0 variable)
%            Maximal formula depth :    5 (   4 average)
%            Number of connectives :    6 (   2   ~;   2   |;   0   &;   2   @)
%                                         (   0 <=>;   0  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    4 (   4   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    6 (   3   :)
%            Number of variables   :    0 (   0 sgn;   0   !;   0   ?;   0   ^)
%                                         (   0   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(cD,type,(
    cD: $i > $o )).

thf(cR,type,(
    cR: ( $i > $o ) > $o )).

thf(cC,type,(
    cC: $i > $o )).

thf(cTHM620,conjecture,
    ( ( cC != cD )
    | ~ ( cR @ cC )
    | ( cR @ cD ) )).

%------------------------------------------------------------------------------
