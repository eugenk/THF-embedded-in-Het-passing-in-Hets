logic THF

spec LCL7365 =
%------------------------------------------------------------------------------
% File     : LCL736^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Logical Calculi
% Problem  : TPS problem from AC-THMS
% Version  : Especial.
% English  : Related to the axiom of choice.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0935 [Bro09]

% Status   : Theorem
% Rating   : 0.17 v6.2.0, 0.33 v6.1.0, 0.17 v6.0.0, 0.50 v5.5.0, 0.80 v5.4.0, 0.75 v5.2.0, 1.00 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   24 (   0 equality;  23 variable)
%            Maximal formula depth :   12 (   7 average)
%            Number of connectives :   22 (   0   ~;   2   |;   0   &;  17   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :   15 (  15   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :   11 (   0 sgn;   5   !;   6   ?;   0   ^)
%                                         (  11   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(b_type,type,(
    b: $tType )).

thf(cX5310D,conjecture,
    ( ! [Xr_41: ( b > $o ) > b > b > $o,Xr_42: ( b > $o ) > b > b > $o] :
        ( ! [Xx: b > $o] :
          ? [Xy: b,Xw_11: b] :
            ( ( Xr_41 @ Xx @ Xy @ Xw_11 )
            | ( Xr_42 @ Xx @ Xy @ Xw_11 ) )
       => ? [Xf: ( b > $o ) > b] :
          ! [Xx: b > $o] :
          ? [Xw_11: b] :
            ( ( Xr_41 @ Xx @ ( Xf @ Xx ) @ Xw_11 )
            | ( Xr_42 @ Xx @ ( Xf @ Xx ) @ Xw_11 ) ) )
   => ? [Xj: ( b > $o ) > b] :
      ! [Xp: b > $o] :
        ( ? [Xz: b] :
            ( Xp @ Xz )
       => ( Xp @ ( Xj @ Xp ) ) ) )).

%------------------------------------------------------------------------------
