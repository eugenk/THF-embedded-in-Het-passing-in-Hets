logic THF

spec LCL7435 =
%------------------------------------------------------------------------------
% File     : LCL743^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Logical Calculi
% Problem  : TPS problem from AXIOMOFDESCR
% Version  : Especial.
% English  : Related to the axiom of description.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0707 [Bro09]

% Status   : CounterSatisfiable
% Rating   : 0.00 v4.0.0
% Syntax   : Number of formulae    :    5 (   2 unit;   4 type;   0 defn)
%            Number of atoms       :   17 (   1 equality;   2 variable)
%            Maximal formula depth :    6 (   3 average)
%            Number of connectives :    7 (   0   ~;   0   |;   0   &;   4   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    3 (   3   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    6 (   4   :)
%            Number of variables   :    1 (   0 sgn;   1   !;   0   ?;   0   ^)
%                                         (   1   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_CSA_EQU

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cF,type,(
    cF: a > $o )).

thf(cJ,type,(
    cJ: ( a > $o ) > a )).

thf(cX,type,(
    cX: a )).

thf(cDESCR_CHURCH,conjecture,
    ( ( cF @ cX )
   => ( ! [Y: a] :
          ( ( cF @ Y )
         => ( cX = Y ) )
     => ( cF @ ( cJ @ cF ) ) ) )).

%------------------------------------------------------------------------------
