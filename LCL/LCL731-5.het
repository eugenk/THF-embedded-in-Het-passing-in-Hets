logic THF

spec LCL7315 =
%------------------------------------------------------------------------------
% File     : LCL731^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Logical Calculi
% Problem  : TPS problem THM541
% Version  : Especial.
% English  : Equivalence of global choice at type A (usual way of expressing 
%            AC in type theory).

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0461 [Bro09]
%          : THM541 [TPS]

% Status   : Theorem
% Rating   : 0.17 v6.1.0, 0.00 v6.0.0, 0.33 v5.5.0, 0.60 v5.4.0, 0.50 v5.3.0, 0.75 v4.1.0, 0.67 v4.0.0
% Syntax   : Number of formulae    :    2 (   1 unit;   1 type;   0 defn)
%            Number of atoms       :   15 (   0 equality;  14 variable)
%            Maximal formula depth :    9 (   6 average)
%            Number of connectives :   13 (   0   ~;   0   |;   0   &;   8   @)
%                                         (   1 <=>;   4  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    9 (   9   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    3 (   1   :)
%            Number of variables   :    8 (   0 sgn;   4   !;   4   ?;   0   ^)
%                                         (   8   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(a_type,type,(
    a: $tType )).

thf(cTHM541,conjecture,
    ( ? [Xf: ( a > $o ) > a] :
      ! [X: a > $o] :
        ( ? [Xt: a] :
            ( X @ Xt )
       => ( X @ ( Xf @ X ) ) )
  <=> ! [Xs: ( a > $o ) > $o] :
        ( ! [X: a > $o] :
            ( ( Xs @ X )
           => ? [Xt: a] :
                ( X @ Xt ) )
       => ? [Xf: ( a > $o ) > a] :
          ! [X: a > $o] :
            ( ( Xs @ X )
           => ( X @ ( Xf @ X ) ) ) ) )).

%------------------------------------------------------------------------------
