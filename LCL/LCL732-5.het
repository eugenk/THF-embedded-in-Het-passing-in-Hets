logic THF

spec LCL7325 =
%------------------------------------------------------------------------------
% File     : LCL732^5 : TPTP v6.2.0. Released v4.0.0.
% Domain   : Logical Calculi
% Problem  : TPS problem from AC-THMS
% Version  : Especial.
% English  : Related to the axiom of choice.

% Refs     : [Bro09] Brown (2009), Email to Geoff Sutcliffe
% Source   : [Bro09]
% Names    : tps_0795 [Bro09]

% Status   : Theorem
% Rating   : 0.00 v5.3.0, 0.25 v5.2.0, 0.00 v4.0.0
% Syntax   : Number of formulae    :    4 (   2 unit;   3 type;   0 defn)
%            Number of atoms       :   12 (   0 equality;   2 variable)
%            Maximal formula depth :    7 (   4 average)
%            Number of connectives :    7 (   0   ~;   0   |;   0   &;   4   @)
%                                         (   0 <=>;   3  =>;   0  <=;   0 <~>)
%                                         (   0  ~|;   0  ~&;   0  !!;   0  ??)
%            Number of type conns  :    5 (   5   >;   0   *;   0   +;   0  <<)
%            Number of symbols     :    6 (   3   :)
%            Number of variables   :    6 (   4 sgn;   2   !;   4   ?;   0   ^)
%                                         (   6   :;   0  !>;   0  ?*)
%                                         (   0  @-;   0  @+)
% SPC      : TH0_THM_NEQ

% Comments : This problem is from the TPS library. Copyright (c) 2009 The TPS
%            project in the Department of Mathematical Sciences at Carnegie
%            Mellon University. Distributed under the Creative Commons copyleft
%            license: http://creativecommons.org/licenses/by-sa/3.0/
%          : 
%------------------------------------------------------------------------------
thf(b_type,type,(
    b: $tType )).

thf(y,type,(
    y: $i )).

thf(p,type,(
    p: $i > $o )).

thf(cX5310_SUB3,conjecture,
    ( ! [Xx: b > $o] :
      ? [Xy0: b] :
        ( ? [Xx0: $i] :
            ( p @ Xx0 )
       => ( p @ y ) )
   => ? [Xf: ( b > $o ) > b] :
      ! [Xx: b > $o] :
        ( ? [Xx0: $i] :
            ( p @ Xx0 )
       => ( p @ y ) ) )).

%------------------------------------------------------------------------------
